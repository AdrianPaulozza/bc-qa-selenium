from mimesis import Numbers
from datetime import *
from dateutil.relativedelta import relativedelta
from random import randint
import uuid
import requests
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
import configuration.system


class TestConnector:
    class User:

        def __init__(self, barcode = None, adult = True, base_url = configuration.system.base_url):
            self.base_url = base_url

            # Ensure that the correct Test Connector endpoint
            # for each library is selected:
            connectors = {'qa': 'http://app002.core.qa.ec2.corp.pvt:8081/', 'stage': 'http://connector1.stage.bibliocommons.com:8180/'}
            if 'calgary.qa' in base_url:
                self.test_connector_endpoint = connectors['qa'] + "bcils-test-CALGARY"
            elif 'cals.qa' in base_url:
                self.test_connector_endpoint = connectors['qa'] + "bcils-test-CENTRALARKANSAS"
            elif 'pickering.qa' in base_url:
                self.test_connector_endpoint = connectors['qa'] + "bcils-test-ils-PICKERING"
            elif 'kcls.qa' in base_url:
                self.test_connector_endpoint = connectors['qa'] + "bcils-test-ils-WA-KCLS"
            elif 'chipublib.qa' in base_url:
                self.test_connector_endpoint = connectors['qa'] + "bcils-test-ils-IL-CPL"
            elif 'skokielibrary.qa' in base_url:
                self.test_connector_endpoint = connectors['qa'] + "bcils-test-ils-IL-SKOKIE"
            elif 'jocolibrary.qa' in base_url:
                self.test_connector_endpoint = connectors['qa'] + "bcils-test-ils-KS-JOHNSONCOUNTY"
            elif 'hclib.qa' in base_url:
                self.test_connector_endpoint = connectors['qa'] + "bcils-test-ils-MN-HENNEPIN"
            elif 'epltest.stage' in base_url:
                self.test_connector_endpoint = connectors['stage'] + "bcils-test-ils-epltest-20190426135804"
            else:
                self.test_connector_endpoint = None

            if barcode is None:
                self.barcode = str(uuid.uuid4()).replace('-', '')[:19]
                # print(id)
                # self.barcode = id[:19]
            else:
                self.barcode = barcode
            self.pin = ''.join(str(digit) for digit in Numbers().integers(start = 0, end = 9, length = 6))
            now = datetime.now()
            if adult:
                years = randint(13, 85)
                months = randint(0, 11)
            else:
                years = randint(0, 12)
                months = randint(0, 10)
            birthday = (now - relativedelta(years = years, months = months, days = randint(1, 31)))
            self.age_in_years = relativedelta(now, birthday).years
            self.dob = birthday.strftime("%Y-%m-%d")
            self.name = ''

        def create(self, driver, fines=None, checkouts=None):
            # If you want control over Fines added to the user account, instantiate the account like so:
            # TestConnector.User().create(self.driver, fines={'amounts': [<float>], 'bibs': [<string>]})
            # If no `fines` dict is passed in, the account is created with no fines on it:
            if fines is None:
                fines_signal = '$f0'
            else:
                amounts = []
                if 'amounts' in fines:
                    for amount in fines['amounts']:
                        _ = str(amount).replace(".", "")
                        amount_signal = 'a{}'.format(_)
                        amounts.append(amount_signal)
                    amount_signal = '&'.join(amounts)
                else:
                    amount_signal = ''

                bibs = []
                if 'bibs' in fines:
                    for index, bib in enumerate(fines['bibs']):
                        _ = bib[:-3]
                        bib_signal = amounts[index] + 'b{}'.format(_)
                        bibs.append(bib_signal)
                    bib_signal = '&'.join(bibs)
                else:
                    bib_signal = ''

                if len(bibs) > 0:
                    fines_signal = '$f{}{}'.format(len(bibs), bib_signal)
                elif len(bibs) == 0 and len(amounts) > 0:
                    fines_signal = '$f{}{}'.format(len(amounts), amount_signal)

            if checkouts is None:
                checkouts_signal = '$c0'
            else:
                bibs = []
                if 'bibs' in checkouts:
                    for bib in checkouts['bibs']:
                        _ = bib[:-3]
                        bib_signal = 'b{}'.format(_)
                        bibs.append(bib_signal)
                    bib_signal = '&'.join(bibs)
                else:
                    bib_signal = ''

                due_dates = []
                if 'due_dates' in checkouts:
                    for index, due_date in enumerate(checkouts['due_dates']):
                        due_date_signal = bibs[index] + 'd{}'.format(due_date)
                        due_dates.append(due_date_signal)
                    due_date_signal = '&'.join(due_dates)
                else:
                    due_date_signal = ''

                if len(bibs) > 0 and len(due_dates) == 0:
                    checkouts_signal = '$c{}{}'.format(len(bibs), bib_signal)
                elif len(due_dates) > 0:
                    checkouts_signal = '$c{}{}'.format(len(bibs), due_date_signal)

            pin_signals = ['$h0', checkouts_signal, fines_signal]
            if self.dob:
                dob = '$p1b' + self.dob
                pin_signals.append(dob + '&e2019-11-17')

            if pin_signals:
                signals = ''.join(pin_signals)
                create_account_URL = "{}/account/{}/{}{}".format(self.test_connector_endpoint, self.barcode, self.pin, signals)
            else:
                create_account_URL = "{}/account/{}/{}".format(self.test_connector_endpoint, self.barcode, self.pin)
            print(create_account_URL)

            response = requests.get(create_account_URL)
            if response.status_code == 200:
                driver.get("{}/user/login?destination=%2F".format(self.base_url))
                user_name = driver.find_element_by_name('name')
                user_pin = driver.find_element_by_name('user_pin')
                javascript = "arguments[0].setAttribute('value', '{}')".format(self.barcode)
                driver.execute_script(javascript, user_name)
                javascript = "arguments[0].setAttribute('value', '{}')".format(self.pin)
                driver.execute_script(javascript, user_pin)
                driver.find_element_by_name('commit').click()

                WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.ID, 'registration_linking_option_normal')))
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[testid='registration_next_step']")))
                driver.find_element_by_css_selector("[testid='registration_next_step']").click()

                WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, 'user_email')))
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[testid='registration_next_step']")))
                driver.find_element_by_css_selector("[testid='registration_next_step']").click()

                if self.age_in_years >= 13: # Adult
                    WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, 'user_name')))
                    user_name = driver.find_element_by_id('user_name')
                    javascript = "arguments[0].setAttribute('value', '{}')".format("Test_" + self.barcode)
                    driver.execute_script(javascript, user_name)
                    # We run into an odd issue where an error is displayed, that the user name is already
                    # in use... Sending an actual key press seems to help with this (not 100% though)?
                    user_name.send_keys(Keys.SPACE)
                else:
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='user-name-color-select']")))
                    driver.find_element_by_css_selector("div[data-test-id='user-name-color-select']").click()
                    driver.find_element_by_css_selector("div[data-test-id='user-name-color-select'] li[data-original-index='2'] > a > span").click()
                    driver.find_element_by_css_selector("div[data-test-id='user-name-animal-select']").click()
                    driver.find_element_by_css_selector("div[data-test-id='user-name-animal-select'] li[data-original-index='2'] > a > span").click()
                    # During the selection of Color/Animal, the spinner can break the 'Continue'
                    # button flow so we have to wait for it to be out of the way:
                    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "[data-test-id='active-spinner']")))
                    WebDriverWait(driver, 5).until(EC.invisibility_of_element_located((By.CSS_SELECTOR, "[data-test-id='active-spinner']")))

                driver.find_element_by_class_name('cp_pretty_checkbox').click() # 'Accept Terms' checkbox
                accept_terms = driver.find_element_by_css_selector("[testid='checkbox_accept_terms']")
                WebDriverWait(driver, 5).until(EC.element_to_be_selected(accept_terms))
                WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-step='finish']")))
                driver.find_element_by_css_selector("[data-step='finish']").click()
                # The following deals with the 'User Name Taken' error behaviour... This needs to be
                # investigated with Development and ultimately removed as a workaround:
                expectation = EC.text_to_be_present_in_element((By.CSS_SELECTOR, "[data-test-id='registration_header']"), "Your account is now set up!")
                try:
                    WebDriverWait(driver, 10).until(expectation)
                except TimeoutException:
                    if driver.find_element_by_class_name("username_taken").is_displayed():
                        print("'Someone else has this username...' error - check DB using query:")
                        print("SELECT * FROM security_user WHERE username = 'Test_{}'".format(self.barcode))
                        try:
                            driver.find_element_by_css_selector("[data-step='finish']").click()
                            WebDriverWait(driver, 10).until(expectation)
                        except TimeoutException as exception:
                            raise exception

                # cookies_list = driver.get_cookies()
                # for cookie in cookies_list:
                #     if cookie['name'] == 'qa_bc_access_token':
                #         print(cookie['value'])

                # import code
                # code.interact(local=dict(globals(), **locals()))

                driver.delete_all_cookies()

                # print('{:<10s}{:>8s}{:>24s}{:>13s}{:>19s}'.format(self.barcode, self.pin, "Test_" + self.barcode, self.dob, " ({} year(s) old)".format(self.age_in_years)))
                return self
            else:
                raise(RequestException, "The request did not return HTTP 200 - the user account was NOT created.")
