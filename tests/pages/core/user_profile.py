from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .base import BasePage
from pypom import Region

# https://<library>.<environment>.bibliocommons.com/user_profile/{user_id}
class UserProfilePage(BasePage):

    # 'security_user.user_id' in DB
    URL_TEMPLATE = "/user_profile/{user_id}"   # Usage: UserProfilePage(self.driver, base_url, user_id = [UserID]).open()

    _username_header_locator = (By.CSS_SELECTOR, "[testid='user_header_name']")
    _message_locator = (By.CSS_SELECTOR, "[testid='user_profile_message']")
    _top_message_container_locator = (By.CSS_SELECTOR, "[data-test-id='top-message-container']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._username_header_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def message(self):
        return self.find_element(*self._message_locator)

    @property
    def top_message_container(self):
        return self.find_element(*self._top_message_container_locator)

    @property
    def is_top_message_container_displayed(self):
        return self.find_element(*self._top_message_container_locator).is_displayed()

    @property
    def message_modal(self):
        self.wait.until(lambda modal_loaded: self.MessageModal(self).loaded)
        return self.MessageModal(self)

    class MessageModal(Region):

        _root_locator = (By.CSS_SELECTOR, "[data-test-id='modal-container']")
        _body_locator = (By.CSS_SELECTOR, "[data-test-id='user-message-textarea']")
        _send_locator = (By.CSS_SELECTOR, "[data-test-id='button-sendmessage']")
        _cancel_locator = (By.CLASS_NAME, "cancel")

        @property
        def loaded(self):
            return 'modal fade in' in self.root.get_attribute('class')

        @property
        def body(self):
            return self.find_element(*self._body_locator)

        @property
        def send(self):
            return self.find_element(*self._send_locator)

        @property
        def cancel(self):
            return self.find_element(*self._cancel_locator)
