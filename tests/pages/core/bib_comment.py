from selenium.webdriver.common.by import By
from pages.core.base import BasePage

# https://<library>.<environment>.bibliocommons.com/item/ugc/[ItemID]?ugc_id=[CommentID]
class CommentPage(BasePage):

    URL_TEMPLATE = "/item/ugc/{item_id}?ugc_id={comment_id}"   # Usage: Comment(self.driver, base_url, item_id = [ItemID], comment_id = [CommentID]).open()

    _user_id_locator = (By.CSS_SELECTOR, "[testid*='user_card_username']")
    _bib_title_locator = (By.CSS_SELECTOR, "[testid*='bib_link']")
    _bib_author_locator = (By.CSS_SELECTOR, "[testid*='author_search']")
    _bib_jacket_cover_locator = (By.CSS_SELECTOR, "[class='jacketCoverLink'] > img")
    _comment_locator = (By.CSS_SELECTOR, "[id^='comment-']")

    @property
    def user_id(self):
        return self.find_element(*self._user_id_locator)

    @property
    def bib_title(self):
        return self.find_element(*self._bib_title_locator)

    @property
    def bib_author(self):
        return self.find_element(*self._bib_author_locator)

    @property
    def bib_jacket_cover(self):
        return self.find_element(*self._bib_jacket_cover_locator)

    @property
    def comment(self):
        return self.find_element(*self._comment_locator)
