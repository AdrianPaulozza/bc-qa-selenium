from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pypom import Region
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Circulation(Region):

    _root_locator = (By.CLASS_NAME, "circulation_compact")
    _circulation_compact_block = (By.CSS_SELECTOR, ".circ_compact_view [data-js='compact_block']")
    _circulation_stats_locator = (By.CSS_SELECTOR, "[testid='text_holdblock'] .circulation_stats")
    _collection_label_locator = (By.CSS_SELECTOR, "[class='branchInfo'] > div > span[class='label']")
    _collection_value_locator = (By.CSS_SELECTOR, "[class='branchInfo'] > div > span[class='value']")
    _availability_by_location_locator = (By.CSS_SELECTOR, "[data-test-id='availability_more_link']")
    _place_a_hold_physical_locator = (By.CSS_SELECTOR, "div.circulation_compact [data-test-id='button-request-hold']")
    _confirm_hold_button_locator = (By.CSS_SELECTOR, "[data-test-id='button-confirm-branch-hold']")
    _cancel_hold_button_locator = (By.CSS_SELECTOR, "[data-test-id='button-cancel-hold']")
    _circulation_availability_status_locator = (By.CSS_SELECTOR, "[testid='item_availability']")

    _place_a_hold_overdrive_locator = (By.CSS_SELECTOR, "div.circulation_compact [testid*='request_link_']")

    #ebooks
    _inline_digital_checkout_success_locator = (By.CSS_SELECTOR, "[data-test-id='alert-success']")

    @property
    def loaded(self):
        return self.wait.until(lambda element_displayed: self.find_element(*self._circulation_compact_block).is_displayed())

    @property
    def circulation_stats(self):
        return self.find_element(*self._circulation_stats_locator)

    @property
    def collection_label(self):
        return self.find_element(*self._collection_label_locator)

    @property
    def collection_value(self):
        return self.find_element(*self._collection_value_locator)

    @property
    def availability_by_location(self):
        return self.find_element(*self._availability_by_location_locator)

    @property
    def is_place_hold_button_displayed(self):
        try:
            return self.find_element(*self._place_a_hold_physical_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def place_hold(self):
        return self.find_element(*self._place_a_hold_physical_locator)

    @property
    def confirm_hold(self):
        return self.find_element(*self._confirm_hold_button_locator)

    @property
    def cancel_hold(self):
        return self.find_element(*self._cancel_hold_button_locator)

    @property
    def is_cancel_hold_displayed(self):
        try:
            return self.find_element(*self._cancel_hold_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def overdrive_place_hold(self):
        return self.find_element(*self._place_a_hold_overdrive_locator)

    @property
    def is_overdrive_place_hold_displayed(self):
        try:
            return self.find_element(*self._place_a_hold_overdrive_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def digital_checkout_inline_success_notfication(self):
        return self.find_element(*self._inline_digital_checkout_success_locator)

    @property
    def is_digital_inline_success_displayed(self):
        try:
            return self.find_element(*self._inline_digital_checkout_success_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def item_availability_status(self):
        return self.find_element(*self._circulation_availability_status_locator)
