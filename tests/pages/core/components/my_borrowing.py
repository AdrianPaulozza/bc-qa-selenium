from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pypom import Region

class MyBorrowing(Region):
    _root_locator = (By.CLASS_NAME, "cp-my-borrowing-sidebar")
    _renew_checkout_message_locator = (By.CSS_SELECTOR, ".cp-toast-message")

    @property
    def checked_out_side_bar(self):
        return self.CheckedOutSideBar(self)

    class CheckedOutSideBar(Region):
        _all_items_locator = (By.CSS_SELECTOR, ".cp-all-sidebar-link.all")
        _over_due_locator = (By.CSS_SELECTOR, ".cp-checked-out-sidebar-link.overdue")
        _due_later_locator = (By.CSS_SELECTOR, ".cp-checked-out-sidebar-link.out")

        @property
        def all_items(self):
            return self.find_element(*self._all_items_locator)

        @property
        def over_due(self):
            return self.find_element(*self._over_due_locator)

    @property
    def is_renew_checkout_message_displayed(self):
        try:
            return self.find_element(*self._renew_checkout_message_locator).is_displayed()
        except NoSuchElementException:
            return False
