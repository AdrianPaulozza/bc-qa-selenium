from pypom import Region
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


class CheckedOutItem(Region):
    '''/v2/checkedout page'''

    _return_now_locator = (By.CSS_SELECTOR, ".cp-primary-btn.btn-primary")
    _bib_title_locator =  (By.CSS_SELECTOR, "span.title-content")
    _renew_status_locator = (By.CSS_SELECTOR, ".cp-renew-count")

    @property
    def return_now(self):
        return self.find_element(*self._return_now_locator)

    @property
    def bib_title(self):
        return self.find_element(*self._bib_title_locator)

    @property
    def is_renew_status_displayed(self):
        try:
            return self.find_element(*self._renew_status_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def renew_status(self):
        return self.find_element(*self._renew_status_locator)
