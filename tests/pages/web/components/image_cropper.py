from selenium.webdriver.common.by import By
from pypom import Region
from selenium.webdriver.common.action_chains import ActionChains


class ImageCropper(Region):

    _cropper_modal_header_locator = (By.CSS_SELECTOR, "h3[data-key='settings-crop-modal-title']")
    _cropper_locator = (By.CSS_SELECTOR, "#crop-container > div > div > div.cropper-crop-box")
    _cropper_box_locator = (By.CSS_SELECTOR, "#crop-container > div > div > div.cropper-drag-box.cropper-modal.cropper-crop")
    _crop_one_crop_two_locator = (By.CSS_SELECTOR, "div.media-frame-contents > div > h3")
    _crop_image_locator = (By.CSS_SELECTOR, "button[data-key='settings-crop-modal-crop-image']")
    _use_different_image_for_crop_1_and_crop_2_locator = (By.CSS_SELECTOR, "a[data-key='settings-crop-modal-different-image']")
    _cancel_locator = (By.CSS_SELECTOR, "button[data-key='settings-crop-modal-cancel']")
    _next_and_done_locator = (By.CSS_SELECTOR, "button[data-key='settings-crop-modal-next']")
    _previous_locator = (By.CSS_SELECTOR, "button[data-key='settings-crop-modal-previous']")
    _crop_one_preview_locator = (By.CSS_SELECTOR, "img#crop-button-preview1")
    _crop_two_preview_locator = (By.CSS_SELECTOR, "img#crop-button-preview2")
    _crop_point_east_locator = (By.CSS_SELECTOR, "span.cropper-point.point-e")
    _crop_point_south_locator = (By.CSS_SELECTOR, "span.cropper-point.point-s")


    @property
    def is_cropper_modal_visible(self):
        return self.find_element(*self._cropper_modal_header_locator).is_displayed()

    @property
    def is_cropper_box_visible(self):
        return self.find_element(*self._cropper_box_locator).is_displayed()

    @property
    def is_crop_one_visible(self):
        crop_one = self.find_element(*self._crop_one_crop_two_locator)
        if crop_one.get_attribute("textContent") == "Crop 1 (of 2)":
            return True
        else:
            return False

    @property
    def is_crop_two_visible(self):
        crop_one = self.find_element(*self._crop_one_crop_two_locator)
        if crop_one.get_attribute("textContent") == "Crop 2 (of 2)":
            return True
        else:
            return False

    def image_one_crop(self):
        ActionChains(self.driver).drag_and_drop(self.crop_point_east, self.crop_point_south).perform()

    def image_two_crop(self):
        ActionChains(self.driver).drag_and_drop(self.crop_point_east, self.crop_point_south).perform()

    @property
    def crop_image(self):
        return self.find_element(*self._crop_image_locator)

    @property
    def use_different_image_for_crop_1(self):
        return self.find_element(*self._use_different_image_for_crop_1_and_crop_2_locator)

    @property
    def use_different_image_for_crop_2(self):
        return self.find_element(*self._use_different_image_for_crop_1_and_crop_2_locator)

    @property
    def cancel(self):
        return self.find_element(*self._cancel_locator)

    @property
    def previous(self):
        return self.find_element(*self._previous_locator)

    @property
    def next(self):
        return self.find_element(*self._next_and_done_locator)

    @property
    def done(self):
        return self.find_element(*self._next_and_done_locator)

    @property
    def is_crop_one_preview_visible(self):
        return self.find_element(*self._crop_one_preview_locator).is_displayed()

    @property
    def is_crop_two_preview_visible(self):
        return self.find_element(*self._crop_two_preview_locator).is_displayed()

    @property
    def crop_point_east(self):
        return self.find_element(*self._crop_point_east_locator)

    @property
    def crop_point_south(self):
        return self.find_element(*self._crop_point_south_locator)
