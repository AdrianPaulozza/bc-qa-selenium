from pypom import Region
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


class SystemMessages(Region):
    _message_locator = (By.CSS_SELECTOR, ".sys_message>p")
    _close_button_locator = (By.CLASS_NAME, "close_btn")

    @property
    def messages(self):
        return self.find_elements(*self._message_locator)

    @property
    def all_messages_displayed(self):
        try:
            messages = self.find_elements(*self._message_locator)
            for message in messages:
                if message.is_displayed() is False:
                    return False
            return True
        except NoSuchElementException:
            return False

    @property
    def close_message(self):
        return self.find_elements(*self._close_button_locator)
