from selenium.webdriver.common.by import By
from pypom import Region
from pages.web.wpadmin.v3.all_contents_page.all_contents_base import AllContentsBasePage
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys


class AllHeroSlidesPage(AllContentsBasePage):

    _rows_locator = (By.CSS_SELECTOR, "tr[id*=post-]")

    @property
    def rows(self):
        return [Row(self, element) for element in self.find_elements(*self._rows_locator)]

    def search_and_delete(self, slide_title):
        self.search_input.send_keys(slide_title, Keys.RETURN)
        self.rows[0].hover_on_title()
        self.rows[0].delete.click()


class Row(Region):

    _checkbox_locator = (By.CSS_SELECTOR, "th.check-column > input[type='checkbox']")
    _title_locator = (By.CSS_SELECTOR, "a.row-title")
    _post_state_locator = (By.CSS_SELECTOR, "span.post-state")
    _edit_locator = (By.CSS_SELECTOR, "span.edit > a")
    _quick_edit_locator = (By.CSS_SELECTOR, "span > a[class*='editinline']")
    _delete_locator = (By.CSS_SELECTOR, "span.trash > a")

    def hover_on_title(self):
        ActionChains(self.driver).move_to_element(self.title).perform()

    @property
    def checkbox(self):
        return self.find_element(*self._checkbox_locator)

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def post_state(self):
        try:
            return self.find_element(*self._post_state_locator)
        except NoSuchElementException:
            return False

    @property
    def edit(self):
        return self.find_element(*self._edit_locator)

    @property
    def quick_edit(self):
        return self.find_element(*self._quick_edit_locator)

    @property
    def delete(self):
        return self.find_element(*self._delete_locator)
