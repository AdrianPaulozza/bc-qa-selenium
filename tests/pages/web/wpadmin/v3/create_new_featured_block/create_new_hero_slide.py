from selenium.webdriver.common.by import By
from pages.web.wpadmin.v3.create_new_featured_block.new_banner_and_hero_slide_base import BannerHeroSlideBasePage
from utils.selenium_helpers import click
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException


class CreateNewHeroSlidePage(BannerHeroSlideBasePage):

    _name_of_slide_locator = (By.CSS_SELECTOR, "input#fm-bw_hero-0-bw_hero_name-0")
    _slide_link_url_locator = (By.CSS_SELECTOR, "input#fm-bw_hero-0-bw_hero_url-0")
    _desktop_image_locator = (By.CSS_SELECTOR, "button#js--open-pick--desktop")
    _tablet_large_mobile_image_locator = (By.CSS_SELECTOR, "button#js--open-pick--mobile_large")
    _small_mobile_image_locator = (By.CSS_SELECTOR, "button#js--open-pick--mobile_small")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def name_of_slide(self):
        return self.find_element(*self._name_of_slide_locator)

    @property
    def slide_link_url(self):
        return self.find_element(*self._slide_link_url_locator)

    @property
    def desktop_image(self):
        return self.find_element(*self._desktop_image_locator)

    @property
    def tablet_large_mobile_image(self):
        return self.find_element(*self._tablet_large_mobile_image_locator)

    @property
    def small_mobile_image(self):
        return self.find_element(*self._small_mobile_image_locator)

    def create_slide(self, name, link_url, desktop_image, tablet_large_mobile_image, small_mobile_image):
        wait = WebDriverWait(self.driver, 20, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        self.name_of_slide.send_keys(name)
        self.slide_link_url.send_keys(link_url)
        click(self.desktop_image)
        click(self.select_widget_image.upload_files_tab)
        self.insert_media.upload_image.send_keys(desktop_image)
        wait.until(lambda s: self.insert_media.add_image_to_widget_button.is_enabled())
        click(self.insert_media.add_image_to_widget_button)
        click(self.tablet_large_mobile_image)
        click(self.select_widget_image.upload_files_tab)
        self.insert_media.upload_image.send_keys(tablet_large_mobile_image)
        wait.until(lambda s: self.insert_media.add_image_to_widget_button.is_enabled())
        click(self.insert_media.add_image_to_widget_button)
        click(self.small_mobile_image)
        click(self.select_widget_image.upload_files_tab)
        self.insert_media.upload_image.send_keys(small_mobile_image)
        wait.until(lambda s: self.insert_media.add_image_to_widget_button.is_enabled())
        click(self.insert_media.add_image_to_widget_button)
        self.scroll_to_top()
        self.publish.click()
