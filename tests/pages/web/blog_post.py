from pages.web.staff_base import StaffBasePage
from selenium.webdriver.common.by import By


class BlogPostPage(StaffBasePage):

    URL_TEMPLATE = '/blogs/post/{title}'

    _blog_title_locator = (By.CSS_SELECTOR, "header.entry-header.single-header > h1")

    @property
    def blog_title(self):
        return self.find_element(*self._blog_title_locator)
