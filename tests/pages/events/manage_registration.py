from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage


class ManageRegistration(BasePage):

    _registration_tab_locator = (By.CSS_SELECTOR, "[role='presentation']")
    _register_user_locator = (By.CSS_SELECTOR, ".register-button")
    _back_to_event_listing_locator = (By.CSS_SELECTOR, ".primary-link.pull-right")
    _spots_reserved_locator = (By.CSS_SELECTOR, ".spots-reserved")
    _register_without_library_card_locator = (By.CSS_SELECTOR, "input[name='registrationMode']")
    _attendees_dropdown_locator = (By.CSS_SELECTOR, ".chosen-single")
    _attendees_dropdown_selection_locator = (By.CSS_SELECTOR, ".active-result")
    _never_mind_locator = (By.CSS_SELECTOR, ".secondary-link.cancel-button")
    _this_event_full_icon_locator = (By.CSS_SELECTOR, ".glyphicon.glyphicon-remove")
    _name_column_locator = (By.CSS_SELECTOR, ".col-md-3.name-col")
    _contact_column_locator = (By.CSS_SELECTOR, ".col-md-3.contact-col")
    _spots_column_locator = (By.CSS_SELECTOR, ".col-md-1.spots-col")
    _registration_text_fields_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-field.form-control")
    _complete_registration_without_library_card_locator = (By.CSS_SELECTOR, ".progress-button.btn.btn-primary.complete-button")
    _complete_registration_with_library_card_locator = (By.CSS_SELECTOR, ".progress-button.btn.btn-primary.save-button")
    _email_all_registrants_locator = (By.CSS_SELECTOR, ".email-registrants-icon")
    _close_overlay_locator = (By.CSS_SELECTOR, ".glyphicon.glyphicon-remove-2")
    _error_messages_register_without_library_card_locator = (By.CSS_SELECTOR, ".error-messages")
    _registration_completed_message_locator = (By.CSS_SELECTOR, ".registration-confirmation h2")
    _sent_a_confirmation_email_message_locator = (By.CSS_SELECTOR, ".text-muted")
    _email_sent_confirmation_message_locator = (By.CSS_SELECTOR, ".center-block.confirmation-email")
    _edit_resend_email_and_unregister_locator = (By.CSS_SELECTOR, ".events-admin-action")
    _confirm_unregister_user_locator = (By.CSS_SELECTOR, ".unregister-button")
    _barcode_text_field_locator = (By.CSS_SELECTOR, ".lookup-field")
    _library_card_continue_locator = (By.CSS_SELECTOR, ".lookup-button")
    _unregister_locator = By.CSS_SELECTOR, ".primary-link.text-danger.unregister-action"
    _first_name_barcode_locator = (By.CSS_SELECTOR, ".first-name")
    _last_name_barcode_locator = (By.CSS_SELECTOR, ".last-name")
    _email_phone_tool_tip_locator = (By.CSS_SELECTOR, "[data-container='.registration-tooltip-container']")
    _phone_email_radio_button_locator = (By.CSS_SELECTOR, ".radio-inline")
    _return_registration_link_locator = (By.CSS_SELECTOR, ".secondary-link.return-button")
    _error_messages_invalid_phone_locator = (By.CSS_SELECTOR, ".alert.alert-danger")
    _tips_error_message_bullet_points_locator = (By.CSS_SELECTOR, ".tips")
    _invalid_barcode_or_username_error_locator = (By.CSS_SELECTOR, ".alert-danger h5")
    _email_phone_help_bubble_locator = (By.CSS_SELECTOR, ".hidden.tooltip-content")

    @property
    def registration_tabs(self):
        return self.find_elements(*self._registration_tab_locator)

    @property
    def register_user(self):
        return self.find_element(*self._register_user_locator)

    @property
    def is_register_user_displayed(self):
        try:
            return self.find_element(*self._register_user_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def back_to_event_listing(self):
        return self.find_element(*self._back_to_event_listing_locator)

    @property
    def is_back_to_event_listing_displayed(self):
        return self.find_element(*self._back_to_event_listing_locator).is_displayed()

    @property
    def number_of_spots_reserved(self):
        return self.find_element(*self._spots_reserved_locator)

    @property
    def is_number_of_spots_reserved_displayed(self):
        try:
            return self.find_element(*self._spots_reserved_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def register_without_library_card(self):
        return self.find_elements(*self._register_without_library_card_locator)

    @property
    def attendees_dropdown(self):
        return self.find_element(*self._attendees_dropdown_locator)

    @property
    def is_attendees_dropdown_displayed(self):
        try:
            return self.find_element(*self._attendees_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def attendees_dropdown_selection(self):
        return self.find_elements(*self._attendees_dropdown_selection_locator)

    @property
    def never_mind(self):
        return self.find_element(*self._never_mind_locator)

    @property
    def is_never_mind_displayed(self):
        try:
            return self.find_element(*self._never_mind_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_this_event_full_displayed(self):
        try:
            return self.find_element(*self._this_event_full_icon_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def name_column(self):
        return self.find_elements(*self._name_column_locator)

    @property
    def contact_column(self):
        return self.find_elements(*self._contact_column_locator)

    @property
    def spots_reserved_column(self):
        return self.find_elements(*self._spots_column_locator)

    @property
    def registration_text_field(self):
        return self.find_elements(*self._registration_text_fields_locator)

    @property
    def barcode_registration_text_field(self):
        return self.find_element(*self._registration_text_fields_locator)

    @property
    def complete_registration_no_card(self):
        return self.find_element(*self._complete_registration_without_library_card_locator)

    @property
    def is_complete_registration_no_card_displayed(self):
        try:
            return self.find_element(*self._complete_registration_without_library_card_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def complete_registration_with_card(self):
        return self.find_element(*self._complete_registration_with_library_card_locator)

    @property
    def is_complete_registration_with_card_displayed(self):
        try:
            return self.find_element(*self._complete_registration_with_library_card_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_complete_registration_with_card_displayed(self):
        try:
            return self.find_element(*self._complete_registration_with_library_card_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def close_overlay(self):
        return self.find_element(*self._close_overlay_locator)

    @property
    def is_close_overlay_displayed(self):
        try:
            return self.find_element(*self._close_overlay_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def error_message_registration_no_card(self):
        return self.find_elements(*self._error_messages_register_without_library_card_locator)

    @property
    def registration_completed_message(self):
        return self.find_element(*self._registration_completed_message_locator)

    @property
    def is_registration_completed_message_displayed(self):
        try:
            return self.find_element(*self._registration_completed_message_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def confirmation_email_message(self):
        return self.find_element(*self._sent_a_confirmation_email_message_locator)

    @property
    def email_in_confirmation_message(self):
        return self.find_element(*self._email_sent_confirmation_message_locator)

    @property
    def admin_action_buttons(self):
        return self.find_elements(*self._edit_resend_email_and_unregister_locator)

    @property
    def unregister_user(self):
        return self.find_element(*self._unregister_locator)

    @property
    def unregister_user_confirmation(self):
        return self.find_element(*self._confirm_unregister_user_locator)

    @property
    def is_unregister_user_confirmation_displayed(self):
        try:
            return self.find_element(*self._confirm_unregister_user_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def library_card_registration(self):
        return self.find_element(*self._barcode_text_field_locator)

    @property
    def is_library_card_registration_displayed(self):
        try:
            return self.find_element(*self._barcode_text_field_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def library_card_continue_registration(self):
        return self.find_element(*self._library_card_continue_locator)

    @property
    def is_library_card_continue_registration_displayed(self):
        try:
            return self.find_element(*self._library_card_continue_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def first_name_barcode_registration(self):
        return self.find_element(*self._first_name_barcode_locator)

    @property
    def last_name_barcode_registration(self):
        return self.find_element(*self._last_name_barcode_locator)

    @property
    def email_phone_tool_tip(self):
        return self.find_element(*self._email_phone_tool_tip_locator)

    @property
    def is_email_phone_tool_tip_displayed(self):
        try:
            return self.find_element(*self._email_phone_tool_tip_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def phone_email_radio_button(self):
        return self.find_elements(*self._phone_email_radio_button_locator)

    @property
    def return_to_registration_list(self):
        return self.find_element(*self._return_registration_link_locator)

    @property
    def is_return_to_registration_list_displayed(self):
        try:
            return self.find_element(*self._return_registration_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def error_messages_invalid_phone(self):
        return self.find_elements(*self._error_messages_invalid_phone_locator)

    @property
    def tips_for_error(self):
        return self.find_element(*self._tips_error_message_bullet_points_locator)

    @property
    def is_tips_for_error_displayed(self):
        try:
            return self.find_element(*self._tips_error_message_bullet_points_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def invalid_barcode_username_error(self):
        return self.find_element(*self._invalid_barcode_or_username_error_locator)

    @property
    def is_invalid_barcode_username_error_displayed(self):
        try:
            return self.find_element(*self._invalid_barcode_or_username_error_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def email_phone_help_bubble(self):
        return self.find_element(*self._email_phone_help_bubble_locator)
