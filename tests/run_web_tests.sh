#!/usr/bin/env bash

# If allure is not installed in your machine, run 'brew install allure' before running this file
# Enter your repository name as an argument when running the file
# Example: ./run_web_tests.sh bc-qa-selenium-sushant

cd ~/$1

# Check if the allure-report directory exists, and remove it and the results directory if true
if [ -d ~/$1/allure-report ]; then
    rm -rf ~/$1/allure-report
    rm -rf ~/$1/allure-results
fi

for testfolder in tests/functional/web/QA-*
do
    pytest $testfolder --alluredir=allure-results
done

for pythonfile in tests/functional/web/*.py
do
    pytest $pythonfile --alluredir=allure-results
done

allure generate --clean allure-results
allure open allure-report
