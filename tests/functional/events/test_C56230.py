import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
import configuration.system
from mimesis import Person
person = Person('en')
from pages.core.home import HomePage
from pages.events.events_admin import AdminEventPage
from pages.events.home_page_events import EventsHome
from pages.events.manage_registration import ManageRegistration
from selenium.webdriver import ActionChains
from utils.bc_events_api import BCEventsAPIEvents


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56230 - Register Barcodeless Patron")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56230", "TestRail")
class TestC56230:
    def test_c56230(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/events/"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.open()
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        events_api = BCEventsAPIEvents(configuration.system.base_url_events)
        title_name = "Py-Test_Single_Event_with_registration"
        event_id = events_api.insert_event_series("single_event_with_registration", "chipublib", title_name)
        events_api.publish_event_series(event_id)
        new_title_name = events_api.get_event_series_by_id(event_id)

        events_admin = AdminEventPage(self.driver)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)
        events_admin.retry_search_until_results_appear(new_title_name)

        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        manage_registration_button = events_admin.admin_event_action_links[4]
        ActionChains(self.driver).move_to_element(manage_registration_button).perform()
        events_admin.admin_event_action_links[4].click()

        manage_registration = ManageRegistration(self.driver)
        manage_registration.wait.until(lambda s: manage_registration.is_register_user_displayed)

        manage_registration.register_user.click()
        manage_registration.wait.until(lambda s: manage_registration.register_without_library_card[1].is_displayed())
        manage_registration.register_without_library_card[1].click()

        complete_registration_button = manage_registration.complete_registration_no_card
        self.driver.execute_script('arguments[0].scrollIntoView(true);', complete_registration_button)

        manage_registration.wait.until(lambda s: manage_registration.is_complete_registration_no_card_displayed)
        manage_registration.complete_registration_no_card.click()

        registrant_1_first_name = person.name()
        registrant_1_last_name = person.last_name()
        registrant_1_email = person.email()

        manage_registration.wait.until(lambda s: manage_registration.error_message_registration_no_card[2].is_displayed())
        manage_registration.error_message_registration_no_card[0].text.should.equal("First Name is required.")
        manage_registration.error_message_registration_no_card[1].text.should.equal("Last Name is required.")
        manage_registration.error_message_registration_no_card[2].text.should.equal("Invalid email")

        manage_registration.registration_text_field[0].send_keys(registrant_1_first_name)
        manage_registration.complete_registration_no_card.click()
        manage_registration.wait.until(lambda s: manage_registration.error_message_registration_no_card[0].is_displayed())
        manage_registration.error_message_registration_no_card[0].text.should.equal("Last Name is required.")
        manage_registration.error_message_registration_no_card[1].text.should.equal("Invalid email")

        manage_registration.registration_text_field[1].send_keys(registrant_1_last_name)
        manage_registration.complete_registration_no_card.click()
        manage_registration.wait.until(lambda s: manage_registration.error_message_registration_no_card[0].is_displayed())
        manage_registration.error_message_registration_no_card[0].text.should.equal("Invalid email")

        manage_registration.registration_text_field[2].send_keys(registrant_1_email)
        manage_registration.registration_text_field[0].clear()
        manage_registration.registration_text_field[1].clear()
        manage_registration.complete_registration_no_card.click()
        manage_registration.wait.until(lambda s: manage_registration.error_message_registration_no_card[0].is_displayed())
        manage_registration.error_message_registration_no_card[0].text.should.equal("First Name is required.")
        manage_registration.error_message_registration_no_card[1].text.should.equal("Last Name is required.")

        manage_registration.registration_text_field[0].send_keys(registrant_1_first_name)
        manage_registration.registration_text_field[1].send_keys(registrant_1_last_name)
        manage_registration.complete_registration_no_card.click()

        manage_registration.wait.until(lambda s: manage_registration.is_registration_completed_message_displayed)
        manage_registration.registration_completed_message.text.should.equal("Registration completed successfully!")
        manage_registration.confirmation_email_message.text.should.equal("We've sent a confirmation email to")
        manage_registration.email_in_confirmation_message.text.should.equal(registrant_1_email)

        manage_registration.wait.until(lambda s: manage_registration.is_close_overlay_displayed)
        manage_registration.close_overlay.click()

        manage_registration.wait.until(lambda s: manage_registration.is_register_user_displayed)
        manage_registration.name_column[1].text.should.equal("{}, {}".format(registrant_1_last_name, registrant_1_first_name))
        manage_registration.contact_column[1].text.should.equal(registrant_1_email)
        manage_registration.is_register_user_displayed.should.be.true

        manage_registration.wait.until(lambda s: manage_registration.is_back_to_event_listing_displayed)
        manage_registration.back_to_event_listing.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        cancel_button = events_admin.admin_event_action_links[3]
        ActionChains(self.driver).move_to_element(cancel_button).perform()
        events_admin.admin_event_action_links[3].click()
        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)
        events_admin.confirm_delete_button.click()

        events_api.unpublish_event_series(event_id)
        events_api.delete_event_series(event_id)
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()
        events_admin.wait.until(lambda s: events_admin.is_new_revised_is_displayed)
        events_admin.new_revised.click()
        events_admin.wait.until(lambda s: events_admin.no_results_displayed)
        events_admin.assert_is_no_results_displayed.should.be.true
