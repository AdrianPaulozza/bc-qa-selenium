import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
import datetime
from pages.core.home import HomePage
from pages.events.home_page_events import EventsHome
from datetime import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from tenacity import*


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("Google Structure data testing")
@allure.testcase("https://bibliocommons.atlassian.net/browse/EVENT-3117", "Requested through Jira Ticket")
class TestC3117:
    def test_c3117(self):

        self.base_url = "https://chipublib.demo.bibliocommons.com/events/"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.open()
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        home_page_events.all_search_results[2].click()
        self.driver.refresh()

        home_page_events.wait.until(lambda s: home_page_events.is_location_map_displayed)
        home_page_events.wait.until(lambda s: home_page_events.share_or_permalink[1].is_displayed)
        home_page_events.share_or_permalink[1].click()
        event_name = home_page_events.event_name.text
        description = home_page_events.description_field.text
        audience = home_page_events.suitable_field.text
        language = home_page_events.language_field.text
        location_street_address = home_page_events.location_street_address.text
        location_name = home_page_events.location_name.text
        location_city = home_page_events.location_city.text
        location_state = home_page_events.location_province_or_state.text

        page_source = self.driver.page_source

        event_date = home_page_events.event_date.text

        event_time = home_page_events.event_time.text
        if event_time == "All Day":
            event_date_split = event_date.split(",", 1)
            event_date_strip= event_date_split[1].strip()
            event_start_date_modified = datetime.strptime(event_date_strip, "%B %d, %Y")
            event_start_time_date = event_start_date_modified.strftime("%Y-%m-%d")
            event_end_start_time_date = event_start_date_modified.strftime("%Y-%m-%d")
        else:
            split_time = event_time.split(' ', 2)

            start_time = split_time[0]
            event_start = "{} {}".format(event_date, start_time)
            event_start = event_start.split(" ", 1)
            start_time_modified = event_start[1]
            event_start_time_date = datetime.strptime(start_time_modified, "%B %d, %Y %I:%M%p")
            event_start_time_date = event_start_time_date.strftime("%Y-%m-%dT%H:%M:00")

            end_time = split_time[2]
            event_end = "{} {}".format(event_date, end_time)
            event_end_modified = event_end.split(" ", 1)
            event_end_start_time_date = datetime.strptime(event_end_modified[1], "%B %d, %Y %I:%M%p")
            event_end_start_time_date = event_end_start_time_date.strftime("%Y-%m-%dT%H:%M:00")

        # Verifying structured data with data collected above
        self.driver.get("https://search.google.com/structured-data/testing-tool/u/0/")

        code_snippet = self.driver.find_element(By.CSS_SELECTOR, "#new-test-code")
        wait = WebDriverWait(self.driver, 10)
        wait.until(lambda s: code_snippet.is_displayed())
        code_snippet.click()

        code_snippet_box = self.driver.find_element(By.CSS_SELECTOR, '.CodeMirror-empty [autocorrect]')
        self.driver.execute_script("arguments[0].value=arguments[1];", code_snippet_box, page_source)

        run_test = self.driver.find_element(By.CSS_SELECTOR, "#new-test-submit-button")

        # google elements are slow to reconize multiple elements. It tends to recognize that its there
        @retry(stop=stop_after_attempt(10), wait=wait_fixed(5))
        def wait_for_lines():
            count_lines = self.driver.find_elements(By.CSS_SELECTOR, '.CodeMirror-linenumber')
            wait.until(lambda s: count_lines[4].is_displayed)

        wait_for_lines()
        run_test.click()

        @retry(stop=stop_after_attempt(10), wait=wait_fixed(5))
        def wait_for_all_dropdown():
            all_dropdown = self.driver.find_element(By.CSS_SELECTOR, ".mdl-button__ripple-container")
            wait.until(lambda s: all_dropdown.is_displayed())
        wait_for_all_dropdown()

        # Event column
        event_right_column = self.driver.find_elements(By.CSS_SELECTOR, ".mdl-data-table__cell--non-numeric.lTBxed-jyrRxf-eEDwDf")
        event_left_column = self.driver.find_elements(By.CSS_SELECTOR, "td.mdl-data-table__cell--non-numeric.V1ur5d-jyrRxf-eEDwDf")

        # creates a list for the right column which contains the data
        right_column_count = len(event_right_column)
        right_column_text = []
        z = 0
        while z < right_column_count:
            right_column_text.append(event_right_column[z].text)
            z += 1

        # creates a list left column which contains the headings
        left_column_count = len(event_left_column)
        left_column_text = []
        x = 0
        while x < left_column_count:
            left_column_text.append(event_left_column[x].text)
            x += 1

        # the are settings that should never change.
        assert left_column_text[0] == "@type"
        assert right_column_text[0] == "Event"

        event_detail_list = [event_name, event_start_time_date, event_end_start_time_date, description, language, audience, location_name, location_street_address, location_city, location_state, event_end_start_time_date]
        detail_count = 0
        y = 0
        event_detail_list_size = len(event_detail_list)

        for items in right_column_text:
            a = 0
            while a != event_detail_list_size:
                strip_right_column_text = right_column_text[y].replace('\n', '').replace('\r', '').replace(' ', '')
                strip_event_detail_list = event_detail_list[a].replace('\n', '').replace('\r', '').replace(' ', '')
                if strip_right_column_text == strip_event_detail_list:
                    detail_count += 1
                    a += 1
                else:
                    a += 1
            y += 1

        # event details +1 because the event details page has locations repeated twice as the mobile view shows a new
        # field, although its hiddent the google structure data picks it up
        if event_time == "All Day":
            # Double counts the start and end date because its the same so added 4 instead
            assert detail_count == (event_detail_list_size + 4)
        else:

            assert detail_count == (event_detail_list_size + 1)
