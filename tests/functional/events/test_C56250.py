import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
import configuration.system
import configuration.user
from mimesis import Person
from pages.core.home import HomePage
from pages.events.events_admin import AdminEventPage
from pages.events.home_page_events import EventsHome
from pages.events.manage_registration import ManageRegistration
from selenium.webdriver import ActionChains
from utils.bc_events_api import BCEventsAPIEvents


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56250 - Event Registration for Authorized (barcode)User: With Email")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56250", "TestRail")
class TestC56250:
    def test_c56250(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/events/"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.open()
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        events_api = BCEventsAPIEvents(configuration.system.base_url_events)
        title_name = "Py-Test_barcode_email"
        event_id = events_api.insert_event_series("single_event_with_registration", "chipublib", title_name)
        events_api.publish_event_series(event_id)
        new_title_name = events_api.get_event_series_by_id(event_id)

        events_admin = AdminEventPage(self.driver)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)
        events_admin.retry_search_until_results_appear(new_title_name)

        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        manage_registration_button = events_admin.admin_event_action_links[4]
        ActionChains(self.driver).move_to_element(manage_registration_button).perform()
        events_admin.admin_event_action_links[4].click()

        manage_registration = ManageRegistration(self.driver)
        manage_registration.wait.until(lambda s: manage_registration.is_number_of_spots_reserved_displayed)
        manage_registration.wait.until(lambda s: manage_registration.is_register_user_displayed)
        manage_registration.register_user.click()

        manage_registration.wait.until(lambda s: manage_registration.is_library_card_registration_displayed)
        barcode = configuration.user.events_barcode_user
        manage_registration.library_card_registration.send_keys(barcode)
        manage_registration.wait.until(lambda s: manage_registration.is_library_card_continue_registration_displayed)
        manage_registration.library_card_continue_registration.click()

        person = Person('en')
        registrant_1_email = person.email()
        manage_registration.wait.until(lambda s: manage_registration.is_email_phone_tool_tip_displayed)
        manage_registration.wait.until(lambda s: manage_registration.barcode_registration_text_field.is_displayed())
        manage_registration.barcode_registration_text_field.clear()
        manage_registration.barcode_registration_text_field.send_keys(registrant_1_email)

        manage_registration.wait.until(lambda s: manage_registration.is_attendees_dropdown_displayed)
        manage_registration.attendees_dropdown.click()
        manage_registration.wait.until(lambda s: manage_registration.attendees_dropdown_selection[9].is_displayed())
        manage_registration.attendees_dropdown_selection[9].click()
        manage_registration.complete_registration_with_card.click()

        manage_registration.wait.until(lambda s: manage_registration.is_registration_completed_message_displayed)
        manage_registration.registration_completed_message.text.should.equal("Registration completed successfully!")
        manage_registration.confirmation_email_message.text.should.equal("We've sent a confirmation email to")
        manage_registration.email_in_confirmation_message.text.should.equal(registrant_1_email)
        manage_registration.close_overlay.click()

        manage_registration.wait.until(lambda s: manage_registration.is_this_event_full_displayed)
        manage_registration.is_this_event_full_displayed.should.be.true
        manage_registration.contact_column[1].text.should.equal(registrant_1_email)

        manage_registration.wait.until(lambda s: manage_registration.is_back_to_event_listing_displayed)
        manage_registration.back_to_event_listing.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.wait.until(lambda s: events_admin.is_admin_event_title_displayed)
        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        cancel_button = events_admin.admin_event_action_links[3]
        ActionChains(self.driver).move_to_element(cancel_button).perform()
        events_admin.wait.until(lambda s: events_admin.admin_event_action_links[3].is_displayed())
        events_admin.admin_event_action_links[3].click()
        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)
        events_admin.confirm_delete_button.click()

        events_api.unpublish_event_series(event_id)
        events_api.delete_event_series(event_id)
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()
        events_admin.wait.until(lambda s: events_admin.is_new_revised_is_displayed)
        events_admin.new_revised.click()
        events_admin.wait.until(lambda s: events_admin.no_results_displayed)
        events_admin.assert_is_no_results_displayed.should.be.true
