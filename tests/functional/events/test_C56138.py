import pytest
import allure
import sys
sys.path.append('tests')
# noinspection PyUnresolvedReferences
import sure
from pages.events.events_locations import EventLocationPage


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("Find by address or ZIP code - valid")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56138", "TestRail")
class TestC56137:
    def test_c56138(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/locations/"
        location_page = EventLocationPage(self.driver, self.base_url).open()

        zip_code = "60605"
        location_page.wait.until(lambda s: location_page.is_find_by_address_or_zip_displayed)
        location_page.find_by_address_or_zip.send_keys(zip_code)
        location_page.search_button[4].click()

        len(location_page.library_in_list_name).should.equal(
            len(location_page.location_search_results_has_get_direction_links))

        location_page.wait.until(lambda s: location_page.location_library_information[1].is_displayed)
        location_page.location_library_information[1].click()
        location_page.wait.until(lambda s: location_page.is_google_pin_expanded_library_name_displayed)
        location_page.wait.until(lambda s: location_page.is_google_pin_expanded_location_address_displayed)
        location_page.is_google_pin_expanded_location_address_displayed.should.be.true
