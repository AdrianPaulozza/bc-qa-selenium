import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
import configuration.system
import configuration.user
from pages.core.home import HomePage
from pages.events.home_page_events import EventsHome
from utils.bc_events_api import BCEventsAPIEvents


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56047 Search Box - search existing")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56047", "TestRail")
class TestC56047:
    def test_c56047(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/events/"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.open()
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        events_api = BCEventsAPIEvents(configuration.system.base_url_events)
        title_name = "Py-Test_search"
        event_id = events_api.insert_event_series("single_event", "chipublib", title_name)
        events_api.publish_event_series(event_id)
        new_title_name = events_api.get_event_series_by_id(event_id)

        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(new_title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: home_page_events.is_event_name_last_word_displayed)
        home_page_events.event_name_last_word.text.should.equal(new_title_name)

        events_api.unpublish_event_series(event_id)
        events_api.delete_event_series(event_id)
        home_page.open()

        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(new_title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: home_page_events.is_search_for_another_displayed)
        home_page_events.is_search_for_another_displayed.should.be.true
