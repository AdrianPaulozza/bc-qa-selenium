import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
    
    # In order for this sample to run, it needs to have an asset that it can associate the campaign to
    example_asset_type = "Email"
    example_asset_item_id = "32798"

    # Retrieve all Campaigns
    print('>>> Retrieve all Campaigns')
    get_camp = FuelSDK.ET_Campaign()
    get_camp.auth_stub = stubObj
    get_response = get_camp.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    if 'count' in get_response.results:
        print('Results(Items) Length: ' + str(len(get_response.results['items'])))
    # print( 'Results(Items): ' + str(get_response.results)
    print('-----------------------------')

    while get_response.more_results:
        print('>>> Continue Retrieve all Campaigns with GetMoreResults')
        get_response = get_camp.getMoreResults()
        print(str(get_response))
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('RequestID: ' + str(get_response.request_id))
        if 'count' in get_response.results:
            print('Results(Items) Length: ' + str(len(get_response.results['items'])))

    # Create a new Campaign
    print('>>> Create a new Campaign')
    post_camp = FuelSDK.ET_Campaign()
    post_camp.auth_stub = stubObj
    post_camp.props = {"name": "PythonSDKCreatedForTest", "description": "PythonSDKCreatedForTest", "color": "FF9933", "favorite": "false"}
    post_response = post_camp.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Results: ' + str(post_response.results))
    print('-----------------------------')

    if post_response.status:
    
        IDOfpostCampaign = post_response.results['id']

        # Retrieve the new Campaign
        print('>>> Retrieve the new Campaign')
        get_camp = FuelSDK.ET_Campaign()
        get_camp.auth_stub = stubObj
        get_camp.props = {"id": IDOfpostCampaign}
        get_response = get_camp.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('Results: ' + str(get_response.results))
        print('-----------------------------')

        # Update the new Campaign
        print('>>> Update the new Campaign')
        patch_camp = FuelSDK.ET_Campaign()
        patch_camp.auth_stub = stubObj
        patch_camp.props = {"id": IDOfpostCampaign, "name": "PythonSDKCreated-Updated!"}
        post_response = patch_camp.patch()
        print('Patch Status: ' + str(post_response.status))
        print('Code: ' + str(post_response.code))
        print('Message: ' + str(post_response.message))
        print('Results: ' + str(post_response.results))
        print('-----------------------------')
    
        # Retrieve the updated Campaign
        print('>>> Retrieve the updated Campaign')
        get_camp = FuelSDK.ET_Campaign()
        get_camp.auth_stub = stubObj
        get_camp.props = {"id": IDOfpostCampaign}
        get_response = get_camp.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('Results: ' + str(get_response.results))
        print('-----------------------------')

        # Create a new Campaign Asset
        print('>>> Create a new Campaign Asset')
        post_camp_asset = FuelSDK.ET_Campaign_Asset()
        post_camp_asset.auth_stub = stubObj
        post_camp_asset.props = {"id": IDOfpostCampaign, "ids": [example_asset_item_id], "type": example_asset_type}
        post_response = post_camp_asset.post()
        print('Post Status: ' + str(post_response.status))
        print('Code: ' + str(post_response.code))
        print('Message: ' + str(post_response.message))
        print('Results: ' + str(post_response.results))
        print('-----------------------------')

        if not isinstance(post_response.results, list):
            quit()
        IDOfpostCampaignAsset = post_response.results[0]['id']

        # Retrieve all Campaign Asset for a campaign
        print('>>> Retrieve all Campaign Asset for a Campaign')
        get_camp_asset = FuelSDK.ET_Campaign_Asset()
        get_camp_asset.auth_stub = stubObj
        get_camp_asset.props = {"id": IDOfpostCampaign}
        get_response = get_camp_asset.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('Results: ' + str(get_response.results))
        print('-----------------------------')

        # Retrieve a single new Campaign Asset
        print('>>> Retrieve a single new Campaign Asset')
        get_camp_asset = FuelSDK.ET_Campaign_Asset()
        get_camp_asset.auth_stub = stubObj
        get_camp_asset.props = {"id": IDOfpostCampaign, "assetId": IDOfpostCampaignAsset}
        get_response = get_camp_asset.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('Results: ' + str(get_response.results))
        print('-----------------------------')

        # Delete the new Campaign Asset
        print('>>> Delete the new Campaign Asset')
        delete_camp_asset = FuelSDK.ET_Campaign_Asset()
        delete_camp_asset.auth_stub = stubObj
        delete_camp_asset.props = {"id": IDOfpostCampaign, "assetId": IDOfpostCampaignAsset}
        delete_response = delete_camp_asset.delete()
        print('Delete Status: ' + str(delete_response.status))
        print('Code: ' + str(delete_response.code))
        print('Message: ' + str(delete_response.message))
        print('Results: ' + str(delete_response.results))
        print('-----------------------------')

        # Get a single a new Campaign Asset to confirm deletion
        print('>>> Get a single a new Campaign Asset to confirm deletion')
        get_camp_asset = FuelSDK.ET_Campaign_Asset()
        get_camp_asset.auth_stub = stubObj
        get_camp_asset.props = {"id": IDOfpostCampaign, "assetId": IDOfpostCampaignAsset}
        get_response = get_camp_asset.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('Results: ' + str(get_response.results))
        print('-----------------------------')

        # Delete the new Campaign
        print('>>> Delete the new Campaign')
        delete_camp = FuelSDK.ET_Campaign()
        delete_camp.auth_stub = stubObj
        delete_camp.props = {"id": IDOfpostCampaign}
        delete_response = delete_camp.delete()
        print('Delete Status: ' + str(delete_response.status))
        print('Code: ' + str(delete_response.code))
        print('Message: ' + str(delete_response.message))
        print('Results: ' + str(delete_response.results))
        print('-----------------------------')

except Exception as e:
    print('Caught exception: ' + str(e))
