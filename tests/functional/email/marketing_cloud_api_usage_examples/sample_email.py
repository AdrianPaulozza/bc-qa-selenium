import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
    
    # Retrieve All Email with GetMoreResults
    print('>>> Retrieve All Email with GetMoreResults')
    get_html_body = FuelSDK.ET_Email()
    get_html_body.auth_stub = stubObj
    get_html_body.props = ["ID", "PartnerKey", "CreatedDate", "ModifiedDate", "Client.ID", "Name", "Folder", "CategoryID", "HTMLBody", "TextBody", "Subject", "IsActive", "IsHTMLPaste", "ClonedFromID",
                         "Status", "EmailType", "CharacterSet", "HasDynamicSubjectLine", "ContentCheckStatus", "Client.PartnerClientKey", "ContentAreas", "CustomerKey"]
    get_response = get_html_body.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    # print 'Results: ' + str(get_response.results)

    while get_response.more_results:
        print('>>> Continue Retrieve All Email with GetMoreResults')
        get_response = get_html_body.getMoreResults()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('RequestID: ' + str(get_response.request_id))
        print('Results Length: ' + str(len(get_response.results)))

    NameOfTestEmail = "PythonSDKEmail"

    # Create Email 
    print('>>> Create Email')
    post_html_body = FuelSDK.ET_Email()
    post_html_body.auth_stub = stubObj
    post_html_body.props = {"CustomerKey": NameOfTestEmail, "Name": NameOfTestEmail, "Subject": "Created Using the PythonSDK", "HTMLBody": "<b>Some HTML Goes here</b>"}
    post_response = post_html_body.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Result Count: ' + str(len(post_response.results)))
    print('Results: ' + str(post_response.results))

    # Retrieve newly created Email
    print('>>> Retrieve newly created Email')
    get_html_body = FuelSDK.ET_Email()
    get_html_body.auth_stub = stubObj
    get_html_body.props = ["ID", "PartnerKey", "CreatedDate", "ModifiedDate", "Client.ID", "Name", "Folder", "CategoryID", "HTMLBody", "TextBody", "Subject", "IsActive", "IsHTMLPaste", "ClonedFromID",
                         "Status", "EmailType", "CharacterSet", "HasDynamicSubjectLine", "ContentCheckStatus", "Client.PartnerClientKey", "ContentAreas", "CustomerKey"]
    get_html_body.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': NameOfTestEmail}
    get_response = get_html_body.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))
    
    # Update Email 
    print('>>> Update Email')
    patch_html_body = FuelSDK.ET_Email()
    patch_html_body.auth_stub = stubObj
    patch_html_body.props = {"CustomerKey": NameOfTestEmail, "Name": NameOfTestEmail, "HTMLBody": "<b>Some HTML HTMLBody Goes here. NOW WITH NEW HTMLBody</b>"}
    patch_response = patch_html_body.patch()
    print('Patch Status: ' + str(patch_response.status))
    print('Code: ' + str(patch_response.code))
    print('Message: ' + str(patch_response.message))
    print('Result Count: ' + str(len(patch_response.results)))
    print('Results: ' + str(patch_response.results))

    # Retrieve updated Email
    print('>>> Retrieve updated Email')
    get_html_body = FuelSDK.ET_Email()
    get_html_body.auth_stub = stubObj
    get_html_body.props = ["ID", "PartnerKey", "CreatedDate", "ModifiedDate", "Client.ID", "Name", "Folder", "CategoryID", "HTMLBody", "TextBody", "Subject", "IsActive", "IsHTMLPaste", "ClonedFromID",
                         "Status", "EmailType", "CharacterSet", "HasDynamicSubjectLine", "ContentCheckStatus", "Client.PartnerClientKey", "ContentAreas", "CustomerKey"]
    get_html_body.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': NameOfTestEmail}
    get_response = get_html_body.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))
    
    # Delete Email 
    print('>>> Delete Email')
    delete_html_body = FuelSDK.ET_Email()
    delete_html_body.auth_stub = stubObj
    delete_html_body.props = {"CustomerKey": NameOfTestEmail, "Name": NameOfTestEmail, "HTMLBody": "<b>Some HTML HTMLBody Goes here. NOW WITH NEW HTMLBody</b>"}
    delete_response = delete_html_body.delete()
    print('Delete Status: ' + str(delete_response.status))
    print('Code: ' + str(delete_response.code))
    print('Message: ' + str(delete_response.message))
    print('Result Count: ' + str(len(delete_response.results)))
    print('Results: ' + str(delete_response.results))
    
    # Retrieve Email to confirm deletion
    print('>>> Retrieve Email to confirm deletion')
    get_html_body = FuelSDK.ET_Email()
    get_html_body.auth_stub = stubObj
    get_html_body.props = ["ID", "PartnerKey", "CreatedDate", "ModifiedDate", "Client.ID", "Name", "Folder", "CategoryID", "HTMLBody", "TextBody", "Subject", "IsActive", "IsHTMLPaste", "ClonedFromID",
                         "Status", "EmailType", "CharacterSet", "HasDynamicSubjectLine", "ContentCheckStatus", "Client.PartnerClientKey", "ContentAreas", "CustomerKey"]
    get_html_body.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': NameOfTestEmail}
    get_response = get_html_body.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

except Exception as e:
    print('Caught exception: ' + str(e))
