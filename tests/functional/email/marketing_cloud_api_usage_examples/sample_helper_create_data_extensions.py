import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
    
    # Example using CreateDataExtensions() method

    # Declare a Python dict which contain all of the details for a DataExtension
    de_one = {"Name": "HelperDEOne", "CustomerKey": "HelperDEOne"}
    de_one['columns'] = [{"Name": "Name", "FieldType": "Text", "IsPrimaryKey": "true", "MaxLength": "100", "IsRequired": "true"},
                         {"Name": "OtherField", "FieldType": "Text"}]
    
    # Declare a 2nd Python dict which contain all of the details for a DataExtension
    de_two = {"Name": "HelperDETwo", "CustomerKey": "HelperDETwo"}
    de_two['columns'] = [{"Name": "Name", "FieldType": "Text", "IsPrimaryKey": "true", "MaxLength": "100", "IsRequired": "true"},
                         {"Name": "OtherField", "FieldType": "Text"}]
    
    # Call CreateDataExtensions passing in both DataExtension Hashes as an Array
    createDEResponse = stubObj.CreateDataExtensions([de_one, de_two])
    print('CreateDataExtensions Status: ' + str(createDEResponse.status))
    print('Code: ' + str(createDEResponse.code))
    print('Message: ' + str(createDEResponse.message))
    print('Results Length: ' + str(len(createDEResponse.results)))
    print('Results: ' + str(createDEResponse.results))
    
    # Cleaning uprint the newly created DEs
    # Delete de_one
    print('>>> Delete de_one')
    de5 = FuelSDK.ET_DataExtension()
    de5.auth_stub = stubObj
    de5.props = {"CustomerKey": "HelperDEOne"}
    del_response = de5.delete()
    print('Delete Status: ' + str(del_response.status))
    print('Code: ' + str(del_response.code))
    print('Message: ' + str(del_response.message))
    print('Results: ' + str(del_response.results))
    
    # Delete de_two
    print('>>> Delete de_two')
    de5 = FuelSDK.ET_DataExtension()
    de5.auth_stub = stubObj
    de5.props = {"CustomerKey": "HelperDETwo"}
    del_response = de5.delete()
    print('Delete Status: ' + str(del_response.status))
    print('Code: ' + str(del_response.code))
    print('Message: ' + str(del_response.message))
    print('Results: ' + str(del_response.results))

except Exception as e:
    print('Caught exception: ' + str(e))
