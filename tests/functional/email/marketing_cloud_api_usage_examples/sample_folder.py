import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
  
    # Retrieve All Folder with GetMoreResults
    print('>>> Retrieve All Folder with GetMoreResults')
    get_folder = FuelSDK.ET_Folder()
    get_folder.auth_stub = stubObj
    get_folder.props = ["ID", "Client.ID", "ParentFolder.ID", "ParentFolder.CustomerKey", "ParentFolder.ObjectID", "ParentFolder.Name", "ParentFolder.Description", "ParentFolder.ContentType", "ParentFolder.IsActive", "ParentFolder.IsEditable", "ParentFolder.AllowChildren", "Name", "Description", "ContentType", "IsActive", "IsEditable", "AllowChildren", "CreatedDate", "ModifiedDate", "Client.ModifiedBy", "ObjectID", "CustomerKey", "Client.EnterpriseID", "Client.CreatedBy"]
    get_response = get_folder.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    # print 'Results: ' + str(get_response.results)

    while get_response.more_results:
        print('>>> Continue Retrieve All Folder with GetMoreResults')
        get_response = get_folder.getMoreResults()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('RequestID: ' + str(get_response.request_id))
        print('Results Length: ' + str(len(get_response.results)))

    name_of_test_folder = "PythonSDKFolder"
    
    # Retrieve Specific Folder for Email Folder ParentID
    print('>>> Retrieve Specific Folder for Email Folder ParentID')
    get_folder = FuelSDK.ET_Folder()
    get_folder.auth_stub = stubObj
    get_folder.props = ["ID"]
    # getFolder.search_filter =  {'Property' : 'ContentType','SimpleOperator' : 'equals','Value' : "email"}
    get_folder.search_filter = {'LeftOperand': {'Property': 'ContentType', 'SimpleOperator': 'equals', 'Value': "email"},
                               'RightOperand': {'Property': 'ParentFolder.ID', 'SimpleOperator': 'equals', 'Value': "0"}, 'LogicalOperator': 'AND'}
    get_response = get_folder.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))
    
    parent_id_for_email = get_response.results[0].ID
    print('Parent Folder for Email: ' + str(parent_id_for_email))

    # Create Folder 
    print('>>> Create Folder')
    post_folder = FuelSDK.ET_Folder()
    post_folder.auth_stub = stubObj
    post_folder.props = {"CustomerKey":  name_of_test_folder, "Name":  name_of_test_folder, "Description":  name_of_test_folder, "ContentType": "EMAIL", "ParentFolder":  {"ID":  parent_id_for_email}}
    post_response = post_folder.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Result Count: ' + str(len(post_response.results)))
    print('Results: ' + str(post_response.results))
  
    # Retrieve newly created Folder
    print('>>> Retrieve newly created Folder')
    get_folder = FuelSDK.ET_Folder()
    get_folder.auth_stub = stubObj
    get_folder.props = ["ID", "Client.ID", "ParentFolder.ID", "ParentFolder.CustomerKey", "ParentFolder.ObjectID", "ParentFolder.Name", "ParentFolder.Description",
                       "ParentFolder.ContentType", "ParentFolder.IsActive", "ParentFolder.IsEditable", "ParentFolder.AllowChildren", "Name", "Description", "ContentType",
                       "IsActive", "IsEditable", "AllowChildren", "CreatedDate", "ModifiedDate", "Client.ModifiedBy", "ObjectID", "CustomerKey", "Client.EnterpriseID", "Client.CreatedBy"]
    get_folder.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': name_of_test_folder}
    get_response = get_folder.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))
    
    # Update Folder 
    print('>>> Update Folder')
    patch_folder = FuelSDK.ET_Folder()
    patch_folder.auth_stub = stubObj
    patch_folder.props = {"CustomerKey":  name_of_test_folder, "Name":  name_of_test_folder, "Description": "Updated Description"}
    patch_response = patch_folder.patch()
    print('Patch Status: ' + str(patch_response.status))
    print('Code: ' + str(patch_response.code))
    print('Message: ' + str(patch_response.message))
    print('Result Count: ' + str(len(patch_response.results)))
    print('Results: ' + str(patch_response.results))
    
    # Retrieve updated Folder
    print('>>> Retrieve updated Folder')
    get_folder = FuelSDK.ET_Folder()
    get_folder.auth_stub = stubObj
    get_folder.props = ["ID", "Client.ID", "ParentFolder.ID", "ParentFolder.CustomerKey", "ParentFolder.ObjectID", "ParentFolder.Name", "ParentFolder.Description", "ParentFolder.ContentType",
                       "ParentFolder.IsActive", "ParentFolder.IsEditable", "ParentFolder.AllowChildren", "Name", "Description", "ContentType", "IsActive", "IsEditable", "AllowChildren",
                       "CreatedDate", "ModifiedDate", "Client.ModifiedBy", "ObjectID", "CustomerKey", "Client.EnterpriseID", "Client.CreatedBy"]
    get_folder.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': name_of_test_folder}
    get_response = get_folder.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))
        
    # Delete Folder 
    print('>>> Delete Folder')
    delete_folder = FuelSDK.ET_Folder()
    delete_folder.auth_stub = stubObj
    delete_folder.props = {"CustomerKey": name_of_test_folder}
    delete_response = delete_folder.delete()
    print('Delete Status: ' + str(delete_response.status))
    print('Code: ' + str(delete_response.code))
    print('Message: ' + str(delete_response.message))
    print('Result Count: ' + str(len(delete_response.results)))
    print('Results: ' + str(delete_response.results))
        
    # Retrieve Folder to confirm deletion
    print('>>> Retrieve Folder to confirm deletion')
    get_folder = FuelSDK.ET_Folder()
    get_folder.auth_stub = stubObj
    get_folder.props = ["ID"]
    get_folder.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': name_of_test_folder}
    get_response = get_folder.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

except Exception as e:
    print('Caught exception: ' + str(e))
