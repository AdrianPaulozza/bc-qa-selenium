import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })

    # NOTE: These examples only work in accounts where the SubscriberKey functionality is not enabled
    #       SubscriberKey will need to be included in the props if that feature is enabled

    subscriber_test_email = "PythonSDKExample@bh.exacttarget.com"

    # Create Subscriber
    print('>>> Create Subscriber')
    post_sub = FuelSDK.ET_Subscriber()
    post_sub.auth_stub = stubObj
    post_sub.props = {"EmailAddress": subscriber_test_email, "SubscriberKey": subscriber_test_email}
    post_response = post_sub.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Result Count: ' + str(len(post_response.results)))
    print('Results: ' + str(post_response.results))

    # Retrieve newly created Subscriber
    print('>>> Retrieve newly created Subscriber')
    get_sub = FuelSDK.ET_Subscriber()
    get_sub.auth_stub = stubObj
    get_sub.props = ["SubscriberKey", "EmailAddress", "Status"]
    get_sub.search_filter = {'Property': 'SubscriberKey', 'SimpleOperator': 'equals', 'Value': subscriber_test_email}
    get_response = get_sub.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

    # Update Subscriber
    print('>>> Update Subscriber')
    patch_sub = FuelSDK.ET_Subscriber()
    patch_sub.auth_stub = stubObj
    patch_sub.props = {"EmailAddress": subscriber_test_email, "Status": "Unsubscribed", "SubscriberKey": subscriber_test_email}
    patch_response = patch_sub.patch()
    print('Patch Status: ' + str(patch_response.status))
    print('Code: ' + str(patch_response.code))
    print('Message: ' + str(patch_response.message))
    print('Result Count: ' + str(len(patch_response.results)))
    print('Results: ' + str(patch_response.results))

    # Retrieve Subscriber that should have status unsubscribed now
    print('>>> Retrieve Subscriber that should have status unsubscribed now')
    get_sub = FuelSDK.ET_Subscriber()
    get_sub.auth_stub = stubObj
    get_sub.props = ["SubscriberKey", "EmailAddress", "Status"]
    get_sub.search_filter = {'Property': 'SubscriberKey', 'SimpleOperator': 'equals', 'Value': subscriber_test_email}
    get_response = get_sub.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

    # Delete Subscriber
    print('>>> Delete Subscriber')
    delete_sub = FuelSDK.ET_Subscriber()
    delete_sub.auth_stub = stubObj
    delete_sub.props = {"EmailAddress": subscriber_test_email, "SubscriberKey": subscriber_test_email}
    delete_response = delete_sub.delete()
    print('Delete Status: ' + str(delete_response.status))
    print('Code: ' + str(delete_response.code))
    print('Message: ' + str(delete_response.message))
    print('Results Length: ' + str(len(delete_response.results)))
    print('Results: ' + str(delete_response.results))

    # Retrieve Subscriber to confirm deletion
    print('>>> Retrieve Subscriber to confirm deletion')
    get_sub = FuelSDK.ET_Subscriber()
    get_sub.auth_stub = stubObj
    get_sub.props = ["SubscriberKey", "EmailAddress", "Status"]
    get_sub.search_filter = {'Property': 'SubscriberKey', 'SimpleOperator': 'equals', 'Value': subscriber_test_email}
    get_response = get_sub.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))


except Exception as e:
    print('Caught exception: ' + str(e))
