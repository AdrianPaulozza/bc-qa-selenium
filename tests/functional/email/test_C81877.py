import pytest
import allure
import sure
import configuration.user
import configuration.system
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_banner import CreateNewBannerPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from mimesis import Text

BANNER_NAME = ' '.join(Text('en').words(quantity=3))
DESKTOP_TITLE = ' '.join(Text('en').words())
MOBILE_TITLE = ' '.join(Text('en').words())


@pytest.mark.email
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C81877: Banner Module - Unsuccessful Banner Creation")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/81877", "TestRail")
class TestC81877:
    def test_C81877(self):
        # Logged in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_email).open()
        login_page.log_in(configuration.user.user['email']['stage']['admin']['name'],
                          configuration.user.user['email']['stage']['admin']['password'])

        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_email, page='bibliocommons-settings', tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        login_page = LoginPage(self.driver, configuration.system.base_url_email).open()
        login_page.log_in(configuration.user.user['email']['stage']['libadmin']['name'],
                          configuration.user.user['email']['stage']['libadmin']['password'])

        create_new_banner = CreateNewBannerPage(self.driver, configuration.system.base_url_email, post_type='bw_banner').open()
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=NoSuchElementException)

        create_new_banner.publish.click()
        wait.until(lambda condition: create_new_banner.error_message(0).is_displayed())

        # The below code can be uncommented once the Web team makes the 'Desktop Banner Title' and 'Mobile Banner Title' mandatory

        # create_new_banner.name_of_banner.send_keys(BANNER_NAME)
        # create_new_banner.scroll_to_top()
        # create_new_banner.publish.click()
        # wait.until(lambda condition: create_new_banner.error_message(1).is_displayed())
        #
        # create_new_banner.name_of_banner.clear()
        # create_new_banner.desktop_title.send_keys(DESKTOP_TITLE)
        # create_new_banner.scroll_to_top()
        # create_new_banner.publish.click()
        # wait.until(lambda condition: create_new_banner.error_message(2).is_displayed())
        #
        # create_new_banner.desktop_title.clear()
        # create_new_banner.mobile_title.send_keys(MOBILE_TITLE)
        # create_new_banner.scroll_to_top()
        # create_new_banner.publish.click()
        # wait.until(lambda condition: create_new_banner.error_message(3).is_displayed())
        #
        # create_new_banner.mobile_title.clear()
        # create_new_banner.name_of_banner.send_keys(BANNER_NAME)
        # create_new_banner.desktop_title.send_keys(DESKTOP_TITLE)
        # create_new_banner.scroll_to_top()
        # create_new_banner.publish.click()
        # wait.until(lambda condition: create_new_banner.error_message(4).is_displayed())
        #
        # create_new_banner.name_of_banner.clear()
        # create_new_banner.mobile_title.send_keys(MOBILE_TITLE)
        # create_new_banner.scroll_to_top()
        # create_new_banner.publish.click()
        # wait.until(lambda condition: create_new_banner.error_message(5).is_displayed())
        #
        # create_new_banner.desktop_title.clear()
        # create_new_banner.name_of_banner.send_keys(BANNER_NAME)
        # create_new_banner.scroll_to_top()
        # create_new_banner.publish.click()
        # wait.until(lambda condition: create_new_banner.error_message(6).is_displayed())
