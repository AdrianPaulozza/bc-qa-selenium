import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text, Internet
from utils.selenium_helpers import click
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_hero_slide import CreateNewHeroSlidePage
from pages.web.user import UserPage
from pages.web.page_builder import PageBuilderPage
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_hero_slides import AllHeroSlidesPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderHeroSlider
from utils.image_download_helper import *


PAGE = 'bibliocommons-settings'
SLIDE_INFO = {
    'name': ' '.join(Text('en').words(quantity=3)),
    'link': Internet('en').home_page()
}
IMAGE_INFO = {
    'desktop_title': '-'.join(Text('en').words(quantity=2)),
    'large_title': '-'.join(Text('en').words(quantity=2)),
    'small_title': '-'.join(Text('en').words(quantity=2))
}
DESKTOP_IMAGE_PATH = get_image_path_name(IMAGE_INFO['desktop_title'], ".jpg")
LARGE_IMAGE_PATH = get_image_path_name(IMAGE_INFO['large_title'], ".jpg")
SMALL_IMAGE_PATH = get_image_path_name(IMAGE_INFO['small_title'], ".jpg")
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Hero Slide"


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87846: Create new block - Hero Slide")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87846", "TestRail")
class TestC87846:
    def test_C87846(self):

        # Log in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])

        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web,
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        # PART 1 - Create a Hero Slide
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        download_image("https://bit.ly/2kucHHR", DESKTOP_IMAGE_PATH)
        download_image("https://bit.ly/2kFap8z", LARGE_IMAGE_PATH)
        download_image("https://bit.ly/2kfgzwc", SMALL_IMAGE_PATH)

        new_hero_slide = CreateNewHeroSlidePage(self.driver, configuration.system.base_url_web, post_type='bw_hero_slide').open()
        new_hero_slide.create_slide(SLIDE_INFO['name'], SLIDE_INFO['link'], DESKTOP_IMAGE_PATH, LARGE_IMAGE_PATH, SMALL_IMAGE_PATH)

        # PART 2 - Verify if it's actually published
        # Verify hero slide is added to All Hero Slides page
        all_hero_slides_page = AllHeroSlidesPage(self.driver, configuration.system.base_url_web, post_type='bw_hero_slide').open()
        all_hero_slides_page.search_content_by_button(SLIDE_INFO['name'])
        all_hero_slides_page.rows[0].title.text.should.match(SLIDE_INFO['name'])

        # Verify image is added to Media Library page with associated content
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search(IMAGE_INFO['desktop_title'])
        media_library_page.rows[0].title.text.should.match(IMAGE_INFO['desktop_title'])
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(SLIDE_INFO['name'])

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search(IMAGE_INFO['large_title'])
        media_library_page.rows[0].title.text.should.match(IMAGE_INFO['large_title'])
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(SLIDE_INFO['name'])

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search(IMAGE_INFO['small_title'])
        media_library_page.rows[0].title.text.should.match(IMAGE_INFO['small_title'])
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(SLIDE_INFO['name'])

        # PART 3 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        if page_staff.builder_panel.is_panel_visible is False:
            page_staff.page_builder.add_content.click()
        page_staff.builder_panel.modules_tab.click()

        # Add Hero Slide
        ActionChains(self.driver).drag_and_drop(page_staff.builder_panel.hero_slider, page_staff.page_builder.body).perform()
        hero_slider = PageBuilderHeroSlider(page_staff)
        hero_slider.select_slide(SLIDE_INFO['name'])
        hero_slider.save()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: len(page_staff.user_facing_modules.hero_sliders) > 0)
        page_staff.page_builder.done_and_publish()
        page_staff.log_out_from_header()

        # PART 4 - Patron view
        self.driver.delete_all_cookies()
        page_patron = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.hero_sliders).should.equal(1)
        page_patron.user_facing_modules.hero_sliders[0].is_more_than_one_slides_displayed.should.be.false
        len(page_patron.user_facing_modules.hero_sliders[0].hero_slides).should.equal(1)
        page_patron.user_facing_modules.hero_sliders[0].hero_slides[0].title.text.should.match(SLIDE_INFO['name'])
        page_patron.user_facing_modules.hero_sliders[0].hero_slides[0].url.get_attribute("href").should.match(SLIDE_INFO['link'])
        wait.until(lambda s: page_staff.user_facing_modules.hero_sliders[0].hero_slides[0].is_desktop_image_displayed.should.be.true)
        page_patron.user_facing_modules.hero_sliders[0].hero_slides[0].desktop_image.get_attribute("src").rsplit('/', 1)[-1].should.match(IMAGE_INFO['desktop_title'])
        page_patron.user_facing_modules.hero_sliders[0].hero_slides[0].large_mobile_image.get_attribute("src").rsplit('/', 1)[-1].should.match(IMAGE_INFO['large_title'])
        page_patron.user_facing_modules.hero_sliders[0].hero_slides[0].small_mobile_image.get_attribute("src").rsplit('/', 1)[-1].should.match(IMAGE_INFO['small_title'])

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_hero_slides_page = AllHeroSlidesPage(self.driver, configuration.system.base_url_web, post_type='bw_hero_slide').open()
        all_hero_slides_page.search_and_delete(SLIDE_INFO['name'])

        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_and_delete(PAGE_TITLE)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_and_delete(IMAGE_INFO['desktop_title'])
        delete_downloaded_image(DESKTOP_IMAGE_PATH)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_and_delete(IMAGE_INFO['large_title'])
        delete_downloaded_image(LARGE_IMAGE_PATH)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_and_delete(IMAGE_INFO['small_title'])

        delete_downloaded_image(SMALL_IMAGE_PATH)
