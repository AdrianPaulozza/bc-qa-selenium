import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text, Internet
from selenium.webdriver.common.keys import Keys
from utils.selenium_helpers import click
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_banner import CreateNewBannerPage
from pages.web.staff_base import StaffBasePage
from pages.web.page_builder import PageBuilderPage
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_banners import AllBannersPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderBanner
from utils.image_download_helper import *
from urllib.parse import urlparse
import random


PAGE = 'bibliocommons-settings'
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Banner"
BANNER_NAME1 = ' '.join(Text('en').words(quantity=3))
BANNER_NAME2 = ' '.join(Text('en').words(quantity=3))
BANNER_NAME3 = ' '.join(Text('en').words(quantity=3))
DESKTOP_TITLE1 = ' '.join(Text('en').words())
DESKTOP_CALLOUT_TEXT1 = ' '.join(Text('en').words(quantity=3))
DESKTOP_CALLOUT_URL1 = Internet('en').home_page()
MOBILE_TITLE1 = ' '.join(Text('en').words())
MOBILE_CALLOUT_TEXT1 = ' '.join(Text('en').words(quantity=3))
MOBILE_CALLOUT_URL1 = Internet('en').home_page()
DESKTOP_DESCRIPTION1 = ' '.join(Text('en').words(quantity=50))
DESKTOP_IMAGE_TITLE1 = ''.join(Text('en').words(quantity=2))
DESKTOP_IMAGE_PATH1 = get_image_path_name(DESKTOP_IMAGE_TITLE1, ".jpg")
DESKTOP_TITLE2 = ' '.join(Text('en').words())
DESKTOP_CALLOUT_TEXT2 = ' '.join(Text('en').words(quantity=3))
DESKTOP_CALLOUT_URL2 = Internet('en').home_page()
MOBILE_TITLE2 = ' '.join(Text('en').words())
MOBILE_CALLOUT_TEXT2 = ' '.join(Text('en').words(quantity=3))
MOBILE_CALLOUT_URL2 = Internet('en').home_page()
DESKTOP_DESCRIPTION2 = ' '.join(Text('en').words(quantity=50))
DESKTOP_IMAGE_TITLE2 = ''.join(Text('en').words(quantity=2))
DESKTOP_IMAGE_PATH2 = get_image_path_name(DESKTOP_IMAGE_TITLE2, ".jpg")
DESKTOP_TITLE3 = ' '.join(Text('en').words())
DESKTOP_CALLOUT_TEXT3 = ' '.join(Text('en').words(quantity=3))
DESKTOP_CALLOUT_URL3 = Internet('en').home_page()
MOBILE_TITLE3 = ' '.join(Text('en').words())
MOBILE_CALLOUT_TEXT3 = ' '.join(Text('en').words(quantity=3))
MOBILE_CALLOUT_URL3 = Internet('en').home_page()
DESKTOP_DESCRIPTION3 = ' '.join(Text('en').words(quantity=50))
DESKTOP_IMAGE_TITLE3 = ''.join(Text('en').words(quantity=2))
DESKTOP_IMAGE_PATH3 = get_image_path_name(DESKTOP_IMAGE_TITLE3, ".jpg")
CALLOUT_STYLE = random.choice(["Solid Button", "Ghost Button", "Link"])
IMAGE_POSITION = random.choice(["Bottom", "Middle", "Top"])
COLOUR_BACKGROUND = "%06x" % random.randint(0, 0xFFFFFF)
COLOUR_TITLE = "%06x" % random.randint(0, 0xFFFFFF)
COLOUR_DESCRIPTION = "%06x" % random.randint(0, 0xFFFFFF)
COLOUR_CALLOUT_TEXT = "%06x" % random.randint(0, 0xFFFFFF)
COLOUR_CALLOUT_BACKGROUND = "%06x" % random.randint(0, 0xFFFFFF)


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87868: PB - Banner")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87868", "TestRail")
class TestC87868:
    def test_C87868(self):
        # Logged in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as Enabled
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.false
        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2kHEzbg", DESKTOP_IMAGE_PATH1)
        download_image("https://bit.ly/2ZFl1o1", DESKTOP_IMAGE_PATH2)
        download_image("https://bit.ly/2rO0q4E", DESKTOP_IMAGE_PATH3)

        # PART 1 - Create new Banner 1
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        new_banner1 = CreateNewBannerPage(self.driver, configuration.system.urls_web[3], post_type='bw_banner').open()
        new_banner1.name_of_banner.send_keys(BANNER_NAME1)
        new_banner1.desktop_title.send_keys(DESKTOP_TITLE1)
        new_banner1.driver.switch_to.frame(new_banner1.desktop_description_iframe)
        new_banner1.desktop_description_body.send_keys(DESKTOP_DESCRIPTION1)
        new_banner1.driver.switch_to.default_content()
        new_banner1.desktop_image.click()
        new_banner1.select_widget_image.upload_files_tab.click()
        new_banner1.insert_media.upload_image.send_keys(DESKTOP_IMAGE_PATH1)
        click(new_banner1.insert_media.add_image_to_widget_button)
        new_banner1.desktop_callout_text.send_keys(DESKTOP_CALLOUT_TEXT1)
        new_banner1.desktop_callout_url.send_keys(DESKTOP_CALLOUT_URL1)
        new_banner1.mobile_title.send_keys(MOBILE_TITLE1)
        new_banner1.mobile_callout_text.send_keys(MOBILE_CALLOUT_TEXT1)
        new_banner1.mobile_callout_url.send_keys(MOBILE_CALLOUT_URL1)
        new_banner1.background_colour.click()
        new_banner1.background_colour_input.send_keys(COLOUR_BACKGROUND)
        new_banner1.title_colour.click()
        new_banner1.title_colour_input.send_keys(COLOUR_TITLE)
        new_banner1.description_colour.click()
        new_banner1.description_colour_input.send_keys(COLOUR_DESCRIPTION)
        new_banner1.select_image_position(IMAGE_POSITION)
        new_banner1.select_callout_style(CALLOUT_STYLE)

        if CALLOUT_STYLE == "Solid Button":
            new_banner1.callout_solid_button.button_background_colour.click()
            new_banner1.callout_solid_button.button_background_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)
            new_banner1.callout_solid_button.button_text_colour.click()
            new_banner1.callout_solid_button.button_text_colour_input.send_keys(COLOUR_CALLOUT_TEXT)
        elif CALLOUT_STYLE == "Ghost Button":
            new_banner1.callout_ghost_button.button_outline_colour.click()
            new_banner1.callout_ghost_button.button_outline_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)
            new_banner1.callout_ghost_button.button_text_hover_colour.click()
            new_banner1.callout_ghost_button.button_text_hover_colour_input.send_keys(COLOUR_CALLOUT_TEXT)
        else:
            new_banner1.callout_link.link_text_colour.click()
            new_banner1.callout_link.link_text_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)

        new_banner1.scroll_to_top()
        new_banner1.publish.click()

        # PART 2 - Create new Banner 2
        new_banner2 = CreateNewBannerPage(self.driver, configuration.system.urls_web[3], post_type='bw_banner').open()
        new_banner2.name_of_banner.send_keys(BANNER_NAME2)
        new_banner2.desktop_title.send_keys(DESKTOP_TITLE2)
        new_banner2.driver.switch_to.frame(new_banner2.desktop_description_iframe)
        new_banner2.desktop_description_body.send_keys(DESKTOP_DESCRIPTION2)
        new_banner2.driver.switch_to.default_content()
        new_banner2.desktop_image.click()
        new_banner2.select_widget_image.upload_files_tab.click()
        new_banner2.insert_media.upload_image.send_keys(DESKTOP_IMAGE_PATH2)
        click(new_banner2.insert_media.add_image_to_widget_button)
        new_banner2.desktop_callout_text.send_keys(DESKTOP_CALLOUT_TEXT2)
        new_banner2.desktop_callout_url.send_keys(DESKTOP_CALLOUT_URL2)
        new_banner2.mobile_title.send_keys(MOBILE_TITLE2)
        new_banner2.mobile_callout_text.send_keys(MOBILE_CALLOUT_TEXT2)
        new_banner2.mobile_callout_url.send_keys(MOBILE_CALLOUT_URL2)
        new_banner2.background_colour.click()
        new_banner2.background_colour_input.send_keys(COLOUR_BACKGROUND)
        new_banner2.title_colour.click()
        new_banner2.title_colour_input.send_keys(COLOUR_TITLE)
        new_banner2.description_colour.click()
        new_banner2.description_colour_input.send_keys(COLOUR_DESCRIPTION)
        new_banner2.select_image_position(IMAGE_POSITION)
        new_banner2.select_callout_style(CALLOUT_STYLE)

        if CALLOUT_STYLE == "Solid Button":
            new_banner2.callout_solid_button.button_background_colour.click()
            new_banner2.callout_solid_button.button_background_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)
            new_banner2.callout_solid_button.button_text_colour.click()
            new_banner2.callout_solid_button.button_text_colour_input.send_keys(COLOUR_CALLOUT_TEXT)
        elif CALLOUT_STYLE == "Ghost Button":
            new_banner2.callout_ghost_button.button_outline_colour.click()
            new_banner2.callout_ghost_button.button_outline_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)
            new_banner2.callout_ghost_button.button_text_hover_colour.click()
            new_banner2.callout_ghost_button.button_text_hover_colour_input.send_keys(COLOUR_CALLOUT_TEXT)
        else:
            new_banner2.callout_link.link_text_colour.click()
            new_banner2.callout_link.link_text_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)

        new_banner2.scroll_to_top()
        new_banner2.publish.click()

        # PART 3 - Create new Banner 3
        new_banner3 = CreateNewBannerPage(self.driver, configuration.system.urls_web[3], post_type='bw_banner').open()
        new_banner3.name_of_banner.send_keys(BANNER_NAME3)
        new_banner3.desktop_title.send_keys(DESKTOP_TITLE3)
        new_banner3.driver.switch_to.frame(new_banner3.desktop_description_iframe)
        new_banner3.desktop_description_body.send_keys(DESKTOP_DESCRIPTION3)
        new_banner3.driver.switch_to.default_content()
        new_banner3.desktop_image.click()
        new_banner3.select_widget_image.upload_files_tab.click()
        new_banner3.insert_media.upload_image.send_keys(DESKTOP_IMAGE_PATH3)
        click(new_banner3.insert_media.add_image_to_widget_button)
        new_banner3.desktop_callout_text.send_keys(DESKTOP_CALLOUT_TEXT3)
        new_banner3.desktop_callout_url.send_keys(DESKTOP_CALLOUT_URL3)
        new_banner3.mobile_title.send_keys(MOBILE_TITLE3)
        new_banner3.mobile_callout_text.send_keys(MOBILE_CALLOUT_TEXT3)
        new_banner3.mobile_callout_url.send_keys(MOBILE_CALLOUT_URL3)
        new_banner3.background_colour.click()
        new_banner3.background_colour_input.send_keys(COLOUR_BACKGROUND)
        new_banner3.title_colour.click()
        new_banner3.title_colour_input.send_keys(COLOUR_TITLE)
        new_banner3.description_colour.click()
        new_banner3.description_colour_input.send_keys(COLOUR_DESCRIPTION)
        new_banner3.select_image_position(IMAGE_POSITION)
        new_banner3.select_callout_style(CALLOUT_STYLE)

        if CALLOUT_STYLE == "Solid Button":
            new_banner3.callout_solid_button.button_background_colour.click()
            new_banner3.callout_solid_button.button_background_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)
            new_banner3.callout_solid_button.button_text_colour.click()
            new_banner3.callout_solid_button.button_text_colour_input.send_keys(COLOUR_CALLOUT_TEXT)
        elif CALLOUT_STYLE == "Ghost Button":
            new_banner3.callout_ghost_button.button_outline_colour.click()
            new_banner3.callout_ghost_button.button_outline_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)
            new_banner3.callout_ghost_button.button_text_hover_colour.click()
            new_banner3.callout_ghost_button.button_text_hover_colour_input.send_keys(COLOUR_CALLOUT_TEXT)
        else:
            new_banner3.callout_link.link_text_colour.click()
            new_banner3.callout_link.link_text_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)

        new_banner3.scroll_to_top()
        new_banner3.publish.click()

        # PART 4 - Verify Banner1 is actually published
        # Verify Banner1 is added to All Banners page
        all_banners_page = AllBannersPage(self.driver, configuration.system.urls_web[3], page='bw-banner').open()
        all_banners_page.search_input.send_keys(BANNER_NAME1)
        all_banners_page.search_button.click()
        all_banners_page.rows[0].title.text.should.match(BANNER_NAME1)
        all_banners_page.rows[0].description.text[:100].should.contain(DESKTOP_DESCRIPTION1[:100])

        # Verify image is added to Media Library page with associated content
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.search_input.send_keys(DESKTOP_IMAGE_TITLE1, Keys.RETURN)
        media_library_page.rows[0].title.text.should.match(DESKTOP_IMAGE_TITLE1)
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(BANNER_NAME1)

        # PART 5 - Verify Banner2 is actually published
        # Verify Banner2 is added to All Banners page
        all_banners_page = AllBannersPage(self.driver, configuration.system.urls_web[3], page='bw-banner').open()
        all_banners_page.search_input.send_keys(BANNER_NAME2)
        all_banners_page.search_button.click()
        all_banners_page.rows[0].title.text.should.match(BANNER_NAME2)
        all_banners_page.rows[0].description.text[:100].should.contain(DESKTOP_DESCRIPTION2[:100])

        # Verify image is added to Media Library page with associated content
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.search_input.send_keys(DESKTOP_IMAGE_TITLE2, Keys.RETURN)
        media_library_page.rows[0].title.text.should.match(DESKTOP_IMAGE_TITLE2)
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(BANNER_NAME2)

        # PART 6 - Verify Banner3 is actually published
        # Verify Banner3 is added to All Banners page
        all_banners_page = AllBannersPage(self.driver, configuration.system.urls_web[3], page='bw-banner').open()
        all_banners_page.search_input.send_keys(BANNER_NAME3)
        all_banners_page.search_button.click()
        all_banners_page.rows[0].title.text.should.match(BANNER_NAME3)
        all_banners_page.rows[0].description.text[:100].should.contain(DESKTOP_DESCRIPTION3[:100])

        # Verify image is added to Media Library page with associated content
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.search_input.send_keys(DESKTOP_IMAGE_TITLE3, Keys.RETURN)
        media_library_page.rows[0].title.text.should.match(DESKTOP_IMAGE_TITLE3)
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(BANNER_NAME3)
        self.driver.set_window_size(1400, 780)

        # PART 7 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        if page_staff.builder_panel.is_panel_visible is False:
            page_staff.wpheader.add_content.click()
        page_staff.builder_panel.modules_tab.click()
        ActionChains(self.driver).drag_and_drop(page_staff.builder_panel.banner, page_staff.page_builder.body).perform()

        # PART 8 - Add all 3 Banners on PB page
        banner = PageBuilderBanner(page_staff)
        banner.content_tab.click()
        banner.wait.until(lambda s: banner.is_banner_placeholder_displayed)
        banner.is_banner_placeholder_displayed.should.be.true
        banner.edit_content_link.click()
        click(banner.edit_banner.choose_banner_dropdown)
        banner.edit_banner.select_banner(BANNER_NAME1)
        click(banner.edit_slide.save)
        banner.wait.until(lambda s: banner.content_tab.is_displayed())
        banner.edit_banner.add_banner.click()
        banner.wait.until(lambda s: banner.edit_content_link.is_displayed())
        banner.edit_content_link.click()
        click(banner.edit_banner.choose_banner_dropdown)
        banner.edit_banner.select_banner(BANNER_NAME2)
        click(banner.edit_slide.save)
        banner.wait.until(lambda s: banner.content_tab.is_displayed())
        banner.edit_banner.add_banner.click()
        banner.wait.until(lambda s: banner.edit_content_link.is_displayed())
        banner.edit_content_link.click()
        click(banner.edit_banner.choose_banner_dropdown)
        banner.edit_banner.select_banner(BANNER_NAME3)
        click(banner.edit_slide.save)
        banner.save()
        wait = WebDriverWait(self.driver, 20, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: page_staff.user_facing_modules.banners[0].is_desktop_image_displayed)
        click(page_staff.page_builder.done)
        page_staff.wait.until(lambda condition: page_staff.page_builder.publish.is_displayed())
        click(page_staff.page_builder.publish)
        wait.until(lambda s: page_staff.wpheader.page_builder.is_displayed())
        page_staff.user_facing_modules.banners[0].desktop_title.text.shouldnt.match(DESKTOP_TITLE2)
        page_staff.user_facing_modules.banners[0].desktop_title.text.shouldnt.match(DESKTOP_TITLE3)
        page_staff.user_facing_modules.banners[0].desktop_title.text.should.match(DESKTOP_TITLE1)
        self.driver.refresh()
        page_staff.user_facing_modules.banners[0].desktop_title.text.shouldnt.match(DESKTOP_TITLE2)
        page_staff.user_facing_modules.banners[0].desktop_title.text.shouldnt.match(DESKTOP_TITLE3)
        page_staff.user_facing_modules.banners[0].desktop_title.text.should.match(DESKTOP_TITLE1)
        self.driver.execute_script("arguments[0].className = 'menupop with-avatar hover'",
                                   page_staff.wpheader.my_account)
        page_staff.wpheader.wait.until(lambda s: page_staff.wpheader.is_my_account_submenu_displayed)
        page_staff.wpheader.my_account_log_out.click()

        # PART 9 - Desktop Banner view
        page_patron = StaffBasePage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.banners).should.equal(1)
        page_patron.user_facing_modules.banners[0].is_desktop_title_displayed.should.be.true
        page_patron.user_facing_modules.banners[0].is_desktop_description_displayed.should.be.true
        page_patron.user_facing_modules.banners[0].is_desktop_image_displayed.should.be.true
        page_patron.user_facing_modules.banners[0].desktop_title.text.shouldnt.match(DESKTOP_TITLE2)
        page_patron.user_facing_modules.banners[0].desktop_title.text.shouldnt.match(DESKTOP_TITLE3)
        page_staff.user_facing_modules.banners[0].desktop_title.text.should.match(DESKTOP_TITLE1)
        page_patron.user_facing_modules.banners[0].desktop_description.text.should.match(DESKTOP_DESCRIPTION1)
        urlparse(page_patron.user_facing_modules.banners[0].desktop_image.get_attribute("src")).path.rsplit('/', 1)[-1].should.match(DESKTOP_IMAGE_TITLE1)

        # PART 10 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_banners_page = AllBannersPage(self.driver, configuration.system.urls_web[3], page='bw-banner').open()
        all_banners_page.search_input.send_keys(BANNER_NAME1, Keys.RETURN)
        all_banners_page.rows[0].hover_on_title()
        all_banners_page.rows[0].delete.click()
        all_banners_page.rows.should.be.empty
        all_banners_page.search_input.clear()
        all_banners_page.search_input.send_keys(BANNER_NAME2, Keys.RETURN)
        all_banners_page.rows[0].hover_on_title()
        all_banners_page.rows[0].delete.click()
        all_banners_page.rows.should.be.empty
        all_banners_page.search_input.clear()
        all_banners_page.search_input.send_keys(BANNER_NAME3, Keys.RETURN)
        all_banners_page.rows[0].hover_on_title()
        all_banners_page.rows[0].delete.click()
        all_banners_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.search_input.send_keys(DESKTOP_IMAGE_TITLE1, Keys.RETURN)
        media_library_page.rows[0].hover_on_title()
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        media_library_page.rows.should.be.empty
        media_library_page.search_input.clear()
        media_library_page.search_input.send_keys(DESKTOP_IMAGE_TITLE2, Keys.RETURN)
        media_library_page.rows[0].hover_on_title()
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        media_library_page.rows.should.be.empty
        media_library_page.search_input.clear()
        media_library_page.search_input.send_keys(DESKTOP_IMAGE_TITLE3, Keys.RETURN)
        media_library_page.rows[0].hover_on_title()
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        media_library_page.rows.should.be.empty

        all_pages_page = AllPagesPage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        all_pages_page.search_input.send_keys(PAGE_TITLE, Keys.RETURN)
        all_pages_page.rows[0].title.text.should.match(PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty

        delete_downloaded_image(DESKTOP_IMAGE_PATH1)
        delete_downloaded_image(DESKTOP_IMAGE_PATH2)
        delete_downloaded_image(DESKTOP_IMAGE_PATH3)
