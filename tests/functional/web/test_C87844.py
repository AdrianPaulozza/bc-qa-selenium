import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.events.home_page_events import EventsHome
from pages.web.wpadmin.v3.create_a_new_card.create_new_event_card import CreateNewEventCard
from utils.image_download_helper import *
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from utils.selenium_helpers import click, enter_text
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage


PAGE = "bibliocommons-settings"
EVENT_CARD_INFO = {
    'title': ' '.join(Text('en').words(quantity=3)),
    'description': ' '.join(Text('en').words(quantity=10))
}
EVENTS_BASE_URL = "https://chipublib.stage.bibliocommons.com"
IMAGE_TITLE = '-'.join(Text('en').words(quantity=2))
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Event"


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87844: Create new card - Event Card (Permalink)")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87844", "TestRail")
class TestC87844:
    def test_C87844(self):

        # Log in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.urls_web[2]).open()
        login_page.log_in(configuration.user.user['web']['stage']['admin']['name'],
                          configuration.user.user['web']['stage']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[2],
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        default_settings_page = DefaultSettingsPageBuilderPage(self.driver, configuration.system.urls_web[2],
                                                               post_type='fl-builder-template',
                                                               page='default-page-builder-settings').open()
        default_settings_page.check_all_display_taxonomy_links_checkboxes()
        default_settings_page.save_changes_button_click()

        self.driver.delete_all_cookies()

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.urls_web[2]).open()
        login_page.log_in(configuration.user.user['web']['stage']['libadmin']['name'],
                          configuration.user.user['web']['stage']['libadmin']['password'])

        settings_general_tab = SettingsGeneralTabPage(self.driver, configuration.system.urls_web[2], page=PAGE,
                                                      tab='generic').open()
        click(settings_general_tab.disable_structured_tags)
        settings_general_tab.save_changes.click()

        download_image("https://bit.ly/2EdPMqG", IMAGE_PATH)

        # Fetching an event url
        events_page = EventsHome(self.driver, EVENTS_BASE_URL).open()
        events_page.wait.until(lambda condition: events_page.is_show_more_displayed)
        try:
            event_id = events_page.events[0].title.get_attribute("pathname").split('/')[-1]
            event_url = EVENTS_BASE_URL + "/events/" + event_id
        except NoSuchElementException:
            raise NoSuchElementException

        # Creating a new event card
        new_event_card = CreateNewEventCard(self.driver, configuration.system.urls_web[2], post_type='bw_event').open()
        new_event_card.use_biblioevent_permalink_url.click()
        enter_text(new_event_card.biblioevent_permalink_url_fields.event_url, event_url)
        new_event_card.biblioevent_permalink_url_fields.grab_event_info.click()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_event_card.is_validator_message_displayed)
        new_event_card.is_validator_success_message_displayed.should.be.true
        new_event_card.validator_success_message.text.should.match("BiblioEvent information successfully grabbed!")

        new_event_card.biblioevent_permalink_url_fields.card_title.clear()
        new_event_card.biblioevent_permalink_url_fields.card_title.send_keys(EVENT_CARD_INFO['title'])
        new_event_card.biblioevent_permalink_url_fields.card_description.clear()
        new_event_card.biblioevent_permalink_url_fields.card_description.send_keys(EVENT_CARD_INFO['description'])

        # Sometimes, a blank image is pulled in with the event. If so, we remove it and add a new one.
        try:
            wait.until(lambda condition: new_event_card.biblioevent_permalink_url_fields.remove_image.is_displayed())
            new_event_card.biblioevent_permalink_url_fields.remove_image.click()
        except TimeoutException:
            pass
        new_event_card.biblioevent_permalink_url_fields.card_image.click()
        new_event_card.biblioevent_permalink_url_fields.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        click(new_event_card.biblioevent_permalink_url_fields.select_widget_image.add_image_to_widget_button)
        new_event_card.select_default_image_crop_views()
        
        new_event_card.is_free_text_tags_field_displayed.should.be.true
        num_of_taxonomies = 3
        for i in range(num_of_taxonomies):
            tag = Text('en').words(quantity=1)[0]
            new_event_card.free_text_tags.send_keys(tag)
            new_event_card.add_free_text_tags.click()
        new_event_card.are_free_text_tags_added.should.be.true

        new_event_card.scroll_to_top()
        new_event_card.publish.click()

        # Creating a new page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.urls_web[2], post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Select single card module
        new_page_builder_page = PageBuilderPage(self.driver, configuration.system.urls_web[2] + PAGE_TITLE).open()
        new_page_builder_page.wpheader.page_builder.click()
        new_page_builder_page.add_single_card(CONTENT_TYPE, EVENT_CARD_INFO['title'])
        wait.until(lambda s: new_page_builder_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        new_page_builder_page.page_builder.done_and_publish()
        new_page_builder_page.log_out_from_header()

        # PART 4 - Patron view
        self.driver.delete_all_cookies()
        page_patron = UserPage(self.driver, configuration.system.urls_web[2] + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(EVENT_CARD_INFO['title'])
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.true

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.urls_web[2]).open()
        login_page.log_in(configuration.user.user['web']['stage']['libadmin']['name'],
                          configuration.user.user['web']['stage']['libadmin']['password'])

        # Deleting the created page
        all_pages = AllPagesPage(self.driver, configuration.system.urls_web[2], post_type='page').open()
        all_pages.search_and_delete(PAGE_TITLE)
        all_pages.rows.should.be.empty

        # Deleting the created card
        all_cards_page = AllCardsPage(self.driver, configuration.system.urls_web[2], page='bw-content-card').open()
        all_cards_page.search_and_delete(EVENT_CARD_INFO['title'])
        all_cards_page.rows.should.be.empty

        # Deleting the uploaded image
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[2], mode='list').open()
        media_library_page.search_and_delete(IMAGE_TITLE)
        media_library_page.rows.should.be.empty

        delete_downloaded_image(IMAGE_PATH)
