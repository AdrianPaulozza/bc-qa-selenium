import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text
from selenium.common.exceptions import ElementNotInteractableException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.core.search_lists_results import SearchListsResultsPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_list_card import CreateNewListCard
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from utils.selenium_helpers import click, enter_text
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage


PAGE = "bibliocommons-settings"
LIST_INFO = {
    'title': ' '.join(Text('en').words(quantity=3)),
    'description': ' '.join(Text('en').words(quantity=5))
}
CORE_BASE_URL = "https://chipublib.stage.bibliocommons.com"
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "List"
LIST_SEARCH_TERM = Text('en').word()


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87840: Create new card - List Card")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87840", "TestRail")
class TestC87840:
    def test_C87840(self):

        # Log in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web,
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        default_settings_page = DefaultSettingsPageBuilderPage(self.driver, configuration.system.base_url_web,
                                                               post_type='fl-builder-template',
                                                               page='default-page-builder-settings').open()
        default_settings_page.check_all_display_taxonomy_links_checkboxes()
        default_settings_page.save_changes_button_click()

        self.driver.delete_all_cookies()

        # Log in as Lib Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        settings_general_tab = SettingsGeneralTabPage(self.driver, configuration.system.base_url_web, page=PAGE,
                                                      tab='generic').open()
        click(settings_general_tab.disable_structured_tags)
        settings_general_tab.save_changes.click()

        # Fetch a list from Core
        search_lists_page = SearchListsResultsPage(self.driver, CORE_BASE_URL, query=LIST_SEARCH_TERM).open()
        try:
            list_href = search_lists_page.lists[0].title.get_attribute("pathname")
            list_url = CORE_BASE_URL + list_href
        except NoSuchElementException:
            raise NoSuchElementException

        # Create a new List card
        new_list_card = CreateNewListCard(self.driver, configuration.system.base_url_web, post_type='bw_list').open()
        enter_text(new_list_card.list_url, list_url)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, ElementNotInteractableException])

        # Adding an extra check to ensure that the list url is properly entered into the field.
        wait.until(lambda condition: list_url in new_list_card.list_url.get_attribute("value"))
        new_list_card.grab_list_info.click()
        wait.until(lambda condition: "List information successfully grabbed!" in self.driver.page_source)
        wait.until(lambda condition: new_list_card.image_cropper.is_crop_one_preview_visible)
        new_list_card.card_title.clear()
        new_list_card.card_title.send_keys(LIST_INFO['title'])
        new_list_card.card_description.clear()
        new_list_card.card_description.send_keys(LIST_INFO['description'])
        new_list_card.scroll_to_top()
        new_list_card.publish.click()

        # Creating a new page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Adding a single card to the page from Page Builder
        new_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_page.wpheader.page_builder.click()
        new_page.add_single_card(CONTENT_TYPE, LIST_INFO['title'])
        wait.until(lambda s: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        new_page.page_builder.done_and_publish()
        new_page.log_out_from_header()

        # PART 4 - Patron view
        self.driver.delete_all_cookies()
        page_patron = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        len(page_patron.user_facing_modules.single_cards[0].cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(LIST_INFO['title'])
        new_page.user_facing_modules.single_cards[0].cards[0].card_description.text.should.equal(LIST_INFO['description'])
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.true
        page_patron.user_facing_modules.single_cards[0].cards[0].card_content_type.text.should.match(CONTENT_TYPE.upper())

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # Deleting the created card
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_and_delete(LIST_INFO['title'])
        all_cards_page.rows.should.be.empty

        # Deleting the created page
        all_pages = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages.search_and_delete(PAGE_TITLE)
        all_pages.rows.should.be.empty
