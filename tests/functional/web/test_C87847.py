import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text, Internet
from selenium.webdriver.common.keys import Keys
from utils.selenium_helpers import click
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_banner import CreateNewBannerPage
from pages.web.staff_base import StaffBasePage
from pages.web.page_builder import PageBuilderPage
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_banners import AllBannersPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderBanner
from utils.image_download_helper import *
from urllib.parse import urlparse
import random


PAGE = 'bibliocommons-settings'
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Banner"
BANNER_NAME = ' '.join(Text('en').words(quantity=3))
DESKTOP_TITLE = ' '.join(Text('en').words())
DESKTOP_CALLOUT_TEXT = ' '.join(Text('en').words(quantity=3))
DESKTOP_CALLOUT_URL = Internet('en').home_page()
MOBILE_TITLE = ' '.join(Text('en').words())
MOBILE_CALLOUT_TEXT = ' '.join(Text('en').words(quantity=3))
MOBILE_CALLOUT_URL = Internet('en').home_page()
DESKTOP_DESCRIPTION = ' '.join(Text('en').words(quantity=50))
DESKTOP_IMAGE_TITLE = ''.join(Text('en').words(quantity=2))
DESKTOP_IMAGE_PATH = get_image_path_name(DESKTOP_IMAGE_TITLE, ".jpg")
CALLOUT_STYLE = random.choice(["Solid Button", "Ghost Button", "Link"])
IMAGE_POSITION = random.choice(["Bottom", "Middle", "Top"])
COLOUR_BACKGROUND = "%06x" % random.randint(0, 0xFFFFFF)
COLOUR_TITLE = "%06x" % random.randint(0, 0xFFFFFF)
COLOUR_DESCRIPTION = "%06x" % random.randint(0, 0xFFFFFF)
COLOUR_CALLOUT_TEXT = "%06x" % random.randint(0, 0xFFFFFF)
COLOUR_CALLOUT_BACKGROUND = "%06x" % random.randint(0, 0xFFFFFF)


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87847: Create new block - Banner")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87847", "TestRail")
class TestC87847:
    def test_C87847(self):
        # Logged in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as Enabled
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page="bibliocommons-settings", tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.false
        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2kHEzbg", DESKTOP_IMAGE_PATH)

        # PART 1 - Create a Banner
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        new_banner = CreateNewBannerPage(self.driver, configuration.system.urls_web[3], post_type='bw_banner').open()
        new_banner.name_of_banner.send_keys(BANNER_NAME)
        new_banner.desktop_title.send_keys(DESKTOP_TITLE)
        new_banner.driver.switch_to.frame(new_banner.desktop_description_iframe)
        new_banner.desktop_description_body.send_keys(DESKTOP_DESCRIPTION)
        new_banner.driver.switch_to.default_content()
        new_banner.desktop_image.click()
        new_banner.select_widget_image.upload_files_tab.click()
        new_banner.insert_media.upload_image.send_keys(DESKTOP_IMAGE_PATH)
        click(new_banner.insert_media.add_image_to_widget_button)
        new_banner.desktop_callout_text.send_keys(DESKTOP_CALLOUT_TEXT)
        new_banner.desktop_callout_url.send_keys(DESKTOP_CALLOUT_URL)
        new_banner.mobile_title.send_keys(MOBILE_TITLE)
        new_banner.mobile_callout_text.send_keys(MOBILE_CALLOUT_TEXT)
        new_banner.mobile_callout_url.send_keys(MOBILE_CALLOUT_URL)
        new_banner.background_colour.click()
        new_banner.background_colour_input.send_keys(COLOUR_BACKGROUND)
        new_banner.title_colour.click()
        new_banner.title_colour_input.send_keys(COLOUR_TITLE)
        new_banner.description_colour.click()
        new_banner.description_colour_input.send_keys(COLOUR_DESCRIPTION)
        new_banner.select_image_position(IMAGE_POSITION)
        new_banner.select_callout_style(CALLOUT_STYLE)

        if CALLOUT_STYLE == "Solid Button":
            new_banner.callout_solid_button.button_background_colour.click()
            new_banner.callout_solid_button.button_background_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)
            new_banner.callout_solid_button.button_text_colour.click()
            new_banner.callout_solid_button.button_text_colour_input.send_keys(COLOUR_CALLOUT_TEXT)
        elif CALLOUT_STYLE == "Ghost Button":
            new_banner.callout_ghost_button.button_outline_colour.click()
            new_banner.callout_ghost_button.button_outline_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)
            new_banner.callout_ghost_button.button_text_hover_colour.click()
            new_banner.callout_ghost_button.button_text_hover_colour_input.send_keys(COLOUR_CALLOUT_TEXT)
        else:
            new_banner.callout_link.link_text_colour.click()
            new_banner.callout_link.link_text_colour_input.send_keys(COLOUR_CALLOUT_BACKGROUND)

        new_banner.scroll_to_top()
        new_banner.publish.click()

        # PART 2 - Verify if it's actually published
        # Verify banner is added to All Banners page
        all_banners_page = AllBannersPage(self.driver, configuration.system.urls_web[3], page='bw-banner').open()
        all_banners_page.search_input.send_keys(BANNER_NAME)
        all_banners_page.search_button.click()
        all_banners_page.rows[0].title.text.should.match(BANNER_NAME)
        all_banners_page.rows[0].description.text[:130].should.contain(DESKTOP_DESCRIPTION[:130])

        # Verify image is added to Media Library page with associated content
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.search_input.send_keys(DESKTOP_IMAGE_TITLE, Keys.RETURN)
        media_library_page.rows[0].title.text.should.match(DESKTOP_IMAGE_TITLE)
        media_library_page.rows[0].uploaded_to_content_title.text.should.match(BANNER_NAME)

        # PART 3 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        if page_staff.builder_panel.is_panel_visible is False:
            page_staff.wpheader.add_content.click()
        page_staff.builder_panel.modules_tab.click()

        # Add Banner
        ActionChains(self.driver).drag_and_drop(page_staff.builder_panel.banner, page_staff.page_builder.body).perform()
        banner = PageBuilderBanner(page_staff)
        banner.edit_content_link.click()
        click(banner.edit_banner.choose_banner_dropdown)
        banner.edit_banner.select_banner(BANNER_NAME)
        banner.edit_slide.save.click()
        banner.save()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: page_staff.user_facing_modules.banners[0].is_desktop_image_displayed)
        click(page_staff.page_builder.done)
        page_staff.wait.until(lambda condition: page_staff.page_builder.publish.is_displayed())
        click(page_staff.page_builder.publish)
        self.driver.execute_script("arguments[0].className = 'menupop with-avatar hover'",
                                   page_staff.wpheader.my_account)
        page_staff.wpheader.wait.until(lambda s: page_staff.wpheader.is_my_account_submenu_displayed)
        page_staff.wpheader.my_account_log_out.click()

        # PART 4 - Patron view
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[3], page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.false
        self.driver.delete_all_cookies()

        page_patron = StaffBasePage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.banners).should.equal(1)

        # Desktop Banner view
        page_patron.user_facing_modules.banners[0].is_desktop_title_displayed.should.be.true
        page_patron.user_facing_modules.banners[0].is_desktop_description_displayed.should.be.true
        page_patron.user_facing_modules.banners[0].is_desktop_image_displayed.should.be.true
        page_patron.user_facing_modules.banners[0].desktop_title.text.should.match(DESKTOP_TITLE)
        page_patron.user_facing_modules.banners[0].desktop_description.text.should.match(DESKTOP_DESCRIPTION)
        urlparse(page_patron.user_facing_modules.banners[0].desktop_image.get_attribute("src")).path.rsplit('/', 1)[-1].should.match(DESKTOP_IMAGE_TITLE)

        if IMAGE_POSITION == 'Top':
            page_patron.user_facing_modules.banners[0].is_image_position_top.should.be.true
        elif IMAGE_POSITION == 'Middle':
            page_patron.user_facing_modules.banners[0].is_image_position_middle.should.be.true
        elif IMAGE_POSITION == 'Bottom':
            page_patron.user_facing_modules.banners[0].is_image_position_bottom.should.be.true

        if CALLOUT_STYLE == "Solid Button":
            page_patron.user_facing_modules.banners[0].is_desktop_callout_link_displayed.should.be.false
            page_patron.user_facing_modules.banners[0].is_desktop_callout_ghost_button_displayed.should.be.false
            page_patron.user_facing_modules.banners[0].is_desktop_callout_solid_button_displayed.should.be.true
            page_patron.user_facing_modules.banners[0].desktop_callout_solid_button.text.should.match(DESKTOP_CALLOUT_TEXT)
            page_patron.user_facing_modules.banners[0].desktop_callout_solid_button.get_attribute("href").should.match(DESKTOP_CALLOUT_URL)
        elif CALLOUT_STYLE == "Ghost Button":
            page_patron.user_facing_modules.banners[0].is_desktop_callout_link_displayed.should.be.false
            page_patron.user_facing_modules.banners[0].is_desktop_callout_solid_button_displayed.should.be.false
            page_patron.user_facing_modules.banners[0].is_desktop_callout_ghost_button_displayed.should.be.true
            page_patron.user_facing_modules.banners[0].desktop_callout_ghost_button.text.should.match(DESKTOP_CALLOUT_TEXT)
            page_patron.user_facing_modules.banners[0].desktop_callout_ghost_button.get_attribute("href").should.match(DESKTOP_CALLOUT_URL)
        else:
            page_patron.user_facing_modules.banners[0].is_desktop_callout_solid_button_displayed.should.be.false
            page_patron.user_facing_modules.banners[0].is_desktop_callout_ghost_button_displayed.should.be.false
            page_patron.user_facing_modules.banners[0].is_desktop_callout_link_displayed.should.be.true
            page_patron.user_facing_modules.banners[0].desktop_callout_link.text.should.match(DESKTOP_CALLOUT_TEXT)
            page_patron.user_facing_modules.banners[0].desktop_callout_link.get_attribute("href").should.match(DESKTOP_CALLOUT_URL)

        # Mobile Banner view
        self.driver.set_window_size(500, 1418)
        self.driver.refresh()
        page_patron.user_facing_modules.banners[0].is_mobile_title_displayed.should.be.true
        page_patron.user_facing_modules.banners[0].mobile_title.text.should.match(MOBILE_TITLE)
        if CALLOUT_STYLE == "Solid Button":
            page_patron.user_facing_modules.banners[0].mobile_callout_solid_button.text.should.match(MOBILE_CALLOUT_TEXT)
            page_patron.user_facing_modules.banners[0].mobile_callout_solid_button.get_attribute("href").should.match(MOBILE_CALLOUT_URL)
        elif CALLOUT_STYLE == "Ghost Button":
            page_patron.user_facing_modules.banners[0].mobile_callout_ghost_button.text.should.match(MOBILE_CALLOUT_TEXT)
            page_patron.user_facing_modules.banners[0].mobile_callout_ghost_button.get_attribute("href").should.match(MOBILE_CALLOUT_URL)
        else:
            page_patron.user_facing_modules.banners[0].mobile_callout_link.text.should.match(MOBILE_CALLOUT_TEXT)
            page_patron.user_facing_modules.banners[0].mobile_callout_link.get_attribute("href").should.match(MOBILE_CALLOUT_URL)

        self.driver.delete_all_cookies()
        self.driver.set_window_size(1400, 1418)

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_banners_page = AllBannersPage(self.driver, configuration.system.urls_web[3], page='bw-banner').open()
        all_banners_page.search_input.send_keys(BANNER_NAME, Keys.RETURN)
        all_banners_page.rows[0].hover_on_title()
        all_banners_page.rows[0].delete.click()
        all_banners_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.search_input.send_keys(DESKTOP_IMAGE_TITLE, Keys.RETURN)
        media_library_page.rows[0].hover_on_title()
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        media_library_page.rows.should.be.empty

        all_pages_page = AllPagesPage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        all_pages_page.search_input.send_keys(PAGE_TITLE, Keys.RETURN)
        all_pages_page.rows[0].title.text.should.match(PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty

        delete_downloaded_image(DESKTOP_IMAGE_PATH)

        self.driver.delete_all_cookies()
