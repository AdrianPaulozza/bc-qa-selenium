import configuration.user
import configuration.system
import pytest
import allure
import sure
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, StaleElementReferenceException
from selenium.webdriver.support.ui import WebDriverWait
from mimesis import Text
from utils.image_download_helper import *
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_new_content.create_new_blog_post import CreateNewBlogPostPage
from pages.web.page_builder import PageBuilderPage
from pages.web.staff_base import StaffBasePage
from pages.web.components.page_builder.page_builder_modules import SideBarMenu, RowModules
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_custom_page import CreateNewCustomPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from selenium.webdriver.common.keys import Keys
from utils.selenium_helpers import click
from pages.web.components.page_builder.page_builder_modules import PageBuilderHorizontalCard


PAGE = "bibliocommons-settings"
BLOG_TITLE = '-'.join(Text('en').words(quantity=3))
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
IMAGE_TITLE = Text('en').words(quantity=1)[0]
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
DESCRIPTION = ' '.join(Text('en').words(quantity=5))


@pytest.mark.usefixtures('selenium_setup_and_teardown')
@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@allure.title("C87879: PB - Add Horizontal Card")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87879", "TestRail")
class TestC87879:
    def test_C87879(self):

        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as Enabled
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        # Logging out
        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2IOmAJs", IMAGE_PATH)

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # PART 1 - Create new Blog Post/Card to be added in
        new_blog_post = CreateNewBlogPostPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        new_blog_post.title.send_keys(BLOG_TITLE)
        new_blog_post.driver.switch_to.frame(new_blog_post.visual_area.iframe)
        new_blog_post.visual_area.body.send_keys(DESCRIPTION)
        new_blog_post.driver.switch_to.default_content()
        new_blog_post.card_image.click()
        new_blog_post.select_widget_image.upload_files_tab.click()
        new_blog_post.insert_media.upload_image.send_keys(IMAGE_PATH)
        click(new_blog_post.insert_media.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_blog_post.image_cropper.is_cropper_modal_visible)
        wait.until(lambda condition: new_blog_post.image_cropper.is_crop_one_visible)
        wait.until(lambda condition: new_blog_post.image_cropper.is_cropper_box_visible)
        new_blog_post.image_cropper.image_one_crop()
        new_blog_post.image_cropper.crop_image.click()
        new_blog_post.image_cropper.next.click()
        wait.until(lambda condition: new_blog_post.image_cropper.is_cropper_box_visible)
        wait.until(lambda condition: new_blog_post.image_cropper.is_crop_one_visible == False)
        wait.until(lambda condition: new_blog_post.image_cropper.is_crop_two_visible)
        new_blog_post.image_cropper.image_two_crop()
        new_blog_post.image_cropper.crop_image.click()
        new_blog_post.image_cropper.done.click()
        wait.until(lambda condition: new_blog_post.image_cropper.is_cropper_modal_visible == False)
        click(new_blog_post.autofill_text_fields)
        new_blog_post.scroll_to_top()
        click(new_blog_post.publish)
        new_blog_post.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Creating a new custom page
        create_new_custom_page = CreateNewCustomPage(self.driver, configuration.system.base_url_web, post_type='page',
                                                     page_type='custom').open()
        create_new_custom_page.title.send_keys(PAGE_TITLE)
        create_new_custom_page.publish.click()
        create_new_custom_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening the new page in the frontend
        new_custom_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_custom_page.wpheader.page_builder.click()
        if new_custom_page.builder_panel.is_panel_visible == False:
            new_custom_page.page_builder.add_content.click()
        new_custom_page.builder_panel.rows_tab.click()

        # Adding a two-columns module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_custom_page.builder_panel.two_columns,
                                                new_custom_page.page_builder.body).perform()

        two_columns = RowModules(new_custom_page)
        wait = WebDriverWait(self.driver, 20, poll_frequency=2, ignored_exceptions=[NoSuchElementException, IndexError, StaleElementReferenceException, ElementNotInteractableException])
        wait.until(lambda condition: two_columns.column_body(0).is_displayed())
        ActionChains(self.driver).move_to_element(two_columns.column_body(0)).click().perform()
        wait.until(lambda condition: two_columns.column_settings.styles_tab.is_displayed())
        two_columns.column_settings.styles_tab.click()
        two_columns.column_settings.styles_tab_contents.select_equalize_heights("Yes")
        two_columns.column_settings.save.click()

        if new_custom_page.builder_panel.is_panel_visible == False:
            new_custom_page.page_builder.add_content.click()
        new_custom_page.builder_panel.modules_tab.click()

        # Adding a sidebar menu to the page from Page Builder
        new_custom_page.builder_panel.scroll_to_bottom()
        ActionChains(self.driver).drag_and_drop(new_custom_page.builder_panel.sidebar_menu, two_columns.column_body(0)).perform()
        sidebar_menu = SideBarMenu(new_custom_page)
        sidebar_menu.content_tab.click()
        sidebar_menu.content_tab_contents.select_a_menu.click()
        sidebar_menu.content_tab_contents.sidebar_menu_results(0).click()
        sidebar_menu.save.click()
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.sidebar_menus) == 1)

        if new_custom_page.builder_panel.is_panel_visible is False:
            new_custom_page.page_builder.add_content.click()
        new_custom_page.builder_panel.modules_tab.click()

        # Adding a Horizontal Card module
        ActionChains(self.driver).drag_and_drop(new_custom_page.builder_panel.horizontal_card, two_columns.column_body(1)).perform()
        horizontal_card = PageBuilderHorizontalCard(new_custom_page)
        wait.until(lambda condition: horizontal_card.is_horizontal_card_placeholder_displayed)
        horizontal_card.is_horizontal_card_placeholder_displayed.should.be.true
        horizontal_card.content_tab.click()
        horizontal_card.edit_content_link.click()
        click(horizontal_card.edit_card.card_type)
        horizontal_card.edit_card.select_card_type("Blog Post")
        horizontal_card.edit_card.choose_card.click()
        horizontal_card.edit_card.select_card_by_title(BLOG_TITLE).click()
        click(horizontal_card.edit_card.save)
        horizontal_card.wait.until(lambda s: horizontal_card.content_tab.is_displayed())
        click(horizontal_card.content_tab)
        click(horizontal_card.display_tab)
        horizontal_card.select_author_information("Hide Author")
        horizontal_card.select_content_type_label("Show Content Type")
        click(horizontal_card.styles_tab)
        horizontal_card.select_image_position("Right")
        horizontal_card.save()
        new_custom_page.wait.until(lambda condition: new_custom_page.page_builder.done.is_displayed())
        click(new_custom_page.page_builder.done)
        new_custom_page.wait.until(lambda condition: new_custom_page.page_builder.publish.is_displayed())
        click(new_custom_page.page_builder.publish)
        wait.until(lambda condition: new_custom_page.wpheader.page_builder.is_displayed())
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.horizontal_cards) == 1)
        new_custom_page.open()
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.horizontal_cards) == 1)
        new_custom_page.user_facing_modules.horizontal_cards[0].cards[0].card_title.get_attribute("textContent").should.equal(BLOG_TITLE)
        new_custom_page.user_facing_modules.horizontal_cards[0].cards[0].is_image_position_right_displayed.should.be.true
        new_custom_page.user_facing_modules.horizontal_cards[0].cards[0].is_card_author_displayed.should.be.false
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.horizontal_cards) == 1)
        self.driver.execute_script("arguments[0].className = 'menupop with-avatar hover'",
                                   new_custom_page.wpheader.my_account)
        new_custom_page.wpheader.wait.until(lambda s: new_custom_page.wpheader.is_my_account_submenu_displayed)
        new_custom_page.wpheader.my_account_log_out.click()

        new_custom_page.open()

        # Patron view
        page_patron = StaffBasePage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.horizontal_cards).should.equal(1)
        new_custom_page.user_facing_modules.horizontal_cards[0].cards[0].card_title.get_attribute("textContent").should.equal(BLOG_TITLE)
        new_custom_page.user_facing_modules.horizontal_cards[0].cards[0].is_image_position_right_displayed.should.be.true
        new_custom_page.user_facing_modules.horizontal_cards[0].cards[0].is_card_author_displayed.should.be.false

        delete_downloaded_image(IMAGE_PATH)

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # Deleting the uploaded images from the media library
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, ElementNotInteractableException])
        wait.until(lambda condition: len(media_library_page.rows) == 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        wait.until(lambda condition: len(media_library_page.rows) == 0)

        # Deleting the created Blog Post
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(BLOG_TITLE)
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(BLOG_TITLE)
        all_cards_page.rows[0].hover_on_title()
        wait.until(lambda condition: all_cards_page.rows[0].delete.is_displayed())
        click(all_cards_page.rows[0].delete)
        all_cards_page.rows.should.be.empty

        # Deleting the created page
        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_input.send_keys(PAGE_TITLE)
        all_pages_page.search_button.click()
        all_pages_page.rows[0].title.get_attribute("textContent").should.match(PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        wait.until(lambda condition: all_pages_page.rows[0].delete.is_displayed())
        click(all_pages_page.rows[0].delete)
        all_pages_page.rows.should.be.empty
