import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from utils.image_download_helper import *

IMAGE_TITLE = Text('en').words(quantity=1)[0]
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
# @allure.title("")
# @allure.testcase("", "TestRail")
class Test002:
    def test_002(self):

        download_image("https://bit.ly/2TUV5oS", IMAGE_PATH)

        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])
        settings_general_tab = SettingsGeneralTabPage(self.driver, configuration.system.base_url_web,
                                                      page='bibliocommons-settings', tab='generic').open()
        settings_general_tab.get_a_library_card_image.click()
        settings_general_tab.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        settings_general_tab.select_widget_image.add_image_to_widget_button.click()
        settings_general_tab.support_library_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.add_image_to_widget_button.click()
        settings_general_tab.blog_card_default_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.add_image_to_widget_button.click()
        settings_general_tab.news_card_default_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.add_image_to_widget_button.click()
        settings_general_tab.save_changes.click()
        settings_general_tab.open()

        # Asserting that image was added to the fields
        settings_general_tab.get_a_library_card_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.title.get_attribute("value").should.be.equal(IMAGE_TITLE)
        settings_general_tab.select_widget_image.close_button.click()
        settings_general_tab.support_library_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.title.get_attribute("value").should.be.equal(IMAGE_TITLE)
        settings_general_tab.select_widget_image.close_button.click()
        settings_general_tab.blog_card_default_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.title.get_attribute("value").should.be.equal(IMAGE_TITLE)
        settings_general_tab.select_widget_image.close_button.click()
        settings_general_tab.news_card_default_image.click()
        settings_general_tab.select_widget_image.select_image_from_list(0).click()
        settings_general_tab.select_widget_image.title.get_attribute("value").should.be.equal(IMAGE_TITLE)

        # Delete the image locally and from Stage
        settings_general_tab.select_widget_image.delete_image_link.click()
        settings_general_tab.select_widget_image.accept_alert_to_delete_image()
        settings_general_tab.select_widget_image.close_button.click()
        delete_downloaded_image(IMAGE_PATH)
