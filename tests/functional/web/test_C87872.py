import pytest
import allure
import sure
import configuration.user
import configuration.system
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, StaleElementReferenceException
from selenium.webdriver.support.ui import WebDriverWait
from mimesis import Text, Internet
from utils.image_download_helper import *
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard
from pages.web.page_builder import PageBuilderPage
from pages.web.components.page_builder.page_builder_modules import SideBarMenu, RowModules, PageBuilderMasonryCardCollection
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_custom_page import CreateNewCustomPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from selenium.webdriver.common.keys import Keys
from utils.selenium_helpers import click

CUSTOM_PAGE_TITLE = ''.join(Text('en').words(quantity=3))
CUSTOM_CARD_ONE_INFO = {
    'title': ''.join(Text('en').words(quantity=3)),
    'url': Internet('en').home_page(),
    'description': ' '.join(Text('en').words(quantity=6))
}
CUSTOM_CARD_TWO_INFO = {
    'title': ''.join(Text('en').words(quantity=2)),
    'url': Internet('en').home_page(),
    'description': ' '.join(Text('en').words(quantity=4))
}
IMAGE_ONE_TITLE = Text('en').word()
IMAGE_ONE_PATH = get_image_path_name(IMAGE_ONE_TITLE, ".jpg")
IMAGE_TWO_TITLE = Text('en').word()
IMAGE_TWO_PATH = get_image_path_name(IMAGE_TWO_TITLE, ".jpg")
TAXONOMIES = ['Audience', 'Related Format']
taxonomies_terms = []


@pytest.mark.usefixtures('selenium_setup_and_teardown')
@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@allure.title("C87872: PB - Add Masonry Collection")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87872", "TestRail")
class TestC87872:
    def test_C87872(self):

        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page="bibliocommons-settings", tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()

        # Logging out
        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2EdPMqG", IMAGE_ONE_PATH)
        download_image("https://bit.ly/2IOmAJs", IMAGE_TWO_PATH)

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # Creating a new custom card
        new_custom_card = CreateNewCustomCard(self.driver, configuration.system.base_url_web,
                                              post_type='bw_custom_card').open()
        new_custom_card.card_title.send_keys(CUSTOM_CARD_ONE_INFO['title'])
        new_custom_card.card_image.click()
        new_custom_card.select_widget_image.upload_image.send_keys(IMAGE_ONE_PATH)
        click(new_custom_card.select_widget_image.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 20, poll_frequency=2, ignored_exceptions=[NoSuchElementException, IndexError, StaleElementReferenceException, ElementNotInteractableException])
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        new_custom_card.image_cropper.image_one_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.next.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible == False)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_visible)
        new_custom_card.image_cropper.image_two_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.done.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible == False)
        new_custom_card.image_cropper.is_crop_one_preview_visible.should.be.true
        new_custom_card.image_cropper.is_crop_two_preview_visible.should.be.true
        new_custom_card.card_url.send_keys(CUSTOM_CARD_ONE_INFO['url'])
        new_custom_card.card_description.send_keys(CUSTOM_CARD_ONE_INFO['description'])
        new_custom_card.select_resource_type("Catalog Search")
        new_custom_card.taxonomy(TAXONOMIES[0]).click()
        taxonomies_terms.append(new_custom_card.taxonomy_terms(TAXONOMIES[0])[0])
        new_custom_card.select_taxonomy(TAXONOMIES[0], taxonomies_terms[0]).click()
        new_custom_card.taxonomy(TAXONOMIES[1]).click()
        taxonomies_terms.append(new_custom_card.taxonomy_terms(TAXONOMIES[1])[0])
        new_custom_card.select_taxonomy(TAXONOMIES[1], taxonomies_terms[1]).click()
        new_custom_card.scroll_to_top()
        new_custom_card.publish.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)
        self.driver.refresh()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)

        # Creating another new custom card
        new_custom_card.open()
        new_custom_card.card_title.send_keys(CUSTOM_CARD_TWO_INFO['title'])
        new_custom_card.card_image.click()
        new_custom_card.select_widget_image.upload_files_tab.click()
        new_custom_card.select_widget_image.upload_image.send_keys(IMAGE_TWO_PATH)
        click(new_custom_card.select_widget_image.add_image_to_widget_button)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        new_custom_card.image_cropper.image_one_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.next.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible == False)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_visible)
        new_custom_card.image_cropper.image_two_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.done.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible == False)
        new_custom_card.image_cropper.is_crop_one_preview_visible.should.be.true
        new_custom_card.image_cropper.is_crop_two_preview_visible.should.be.true
        new_custom_card.card_url.send_keys(CUSTOM_CARD_TWO_INFO['url'])
        new_custom_card.card_description.send_keys(CUSTOM_CARD_TWO_INFO['description'])
        new_custom_card.select_resource_type("Catalog Search")
        new_custom_card.taxonomy(TAXONOMIES[0]).click()
        taxonomies_terms.append(new_custom_card.taxonomy_terms(TAXONOMIES[0])[0])
        new_custom_card.select_taxonomy(TAXONOMIES[0], taxonomies_terms[0]).click()
        new_custom_card.taxonomy(TAXONOMIES[1]).click()
        taxonomies_terms.append(new_custom_card.taxonomy_terms(TAXONOMIES[1])[0])
        new_custom_card.select_taxonomy(TAXONOMIES[1], taxonomies_terms[1]).click()
        new_custom_card.scroll_to_top()
        new_custom_card.publish.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)
        self.driver.refresh()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)

        delete_downloaded_image(IMAGE_ONE_PATH)
        delete_downloaded_image(IMAGE_TWO_PATH)

        # Creating a new custom page
        create_new_custom_page = CreateNewCustomPage(self.driver, configuration.system.base_url_web, post_type='page',
                                                     page_type='custom').open()
        create_new_custom_page.title.send_keys(CUSTOM_PAGE_TITLE)
        create_new_custom_page.publish.click()
        create_new_custom_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening the new page in the frontend
        new_custom_page = PageBuilderPage(self.driver, configuration.system.base_url_web + CUSTOM_PAGE_TITLE).open()
        new_custom_page.wpheader.page_builder.click()
        if new_custom_page.builder_panel.is_panel_visible == False:
            new_custom_page.page_builder.add_content.click()
        new_custom_page.builder_panel.rows_tab.click()

        # Adding a two-columns module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_custom_page.builder_panel.two_columns,
                                                new_custom_page.page_builder.body).perform()

        two_columns = RowModules(new_custom_page)
        wait.until(lambda condition: two_columns.column_body(0).is_displayed())
        ActionChains(self.driver).move_to_element(two_columns.column_body(0)).click().perform()
        wait.until(lambda condition: two_columns.column_settings.styles_tab.is_displayed())
        two_columns.column_settings.styles_tab.click()
        two_columns.column_settings.styles_tab_contents.select_equalize_heights("Yes")
        two_columns.column_settings.save.click()

        if new_custom_page.builder_panel.is_panel_visible == False:
            new_custom_page.page_builder.add_content.click()
        new_custom_page.builder_panel.modules_tab.click()

        # Adding a sidebar menu to the page from Page Builder
        new_custom_page.builder_panel.scroll_to_bottom()
        ActionChains(self.driver).drag_and_drop(new_custom_page.builder_panel.sidebar_menu, two_columns.column_body(0)).perform()
        sidebar_menu = SideBarMenu(new_custom_page)
        sidebar_menu.content_tab.click()
        sidebar_menu.content_tab_contents.select_a_menu.click()
        sidebar_menu.content_tab_contents.sidebar_menu_results(0).click()
        sidebar_menu.save.click()
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.sidebar_menus) == 1)

        if new_custom_page.builder_panel.is_panel_visible == False:
            new_custom_page.page_builder.add_content.click()
        new_custom_page.builder_panel.modules_tab.click()

        # Adding a masonry card collection module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_custom_page.builder_panel.masonry_card_collection, two_columns.column_body(1)).perform()
        masonry_card_collection = PageBuilderMasonryCardCollection(new_custom_page)
        wait.until(lambda condition: masonry_card_collection.is_placeholder_displayed)
        masonry_card_collection.content_tab.click()
        masonry_card_collection.module_heading.send_keys("Masonry Card Collection")

        # Selecting the taxonomy terms
        masonry_card_collection.taxonomy(TAXONOMIES[0]).click()
        masonry_card_collection.select_taxonomy(TAXONOMIES[0], taxonomies_terms[0]).click()
        masonry_card_collection.taxonomy(TAXONOMIES[1]).click()
        masonry_card_collection.select_taxonomy(TAXONOMIES[1], taxonomies_terms[1]).click()

        masonry_card_collection.display_tab.click()
        taxonomies_to_display = [masonry_card_collection.audience, masonry_card_collection.related_format]
        for taxonomy in taxonomies_to_display:
            if taxonomy.get_attribute("checked") == "false":
                taxonomy.click()
        taxonomies_to_hide = [masonry_card_collection.programs_and_campaigns, masonry_card_collection.genre, masonry_card_collection.topic, masonry_card_collection.tags]
        for taxonomy in taxonomies_to_hide:
            if taxonomy.get_attribute("checked") == "true":
                taxonomy.click()
        masonry_card_collection.content_tab.click()
        masonry_card_collection.save()
        new_custom_page.page_builder.done.click()
        new_custom_page.wait.until(lambda condition: new_custom_page.page_builder.publish.is_displayed())
        new_custom_page.page_builder.publish.click()
        wait.until(lambda condition: new_custom_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.masonry_card_collections) == 1)
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.sidebar_menus) == 1)
        for index, card in enumerate(new_custom_page.user_facing_modules.masonry_card_collections[0].cards):
            card.card_tags[0].get_attribute("textContent").should.equal(taxonomies_terms[0])
            card.card_tags[1].get_attribute("textContent").should.equal(taxonomies_terms[1])

        new_custom_page.open()

        wait.until(lambda condition: new_custom_page.wpheader.is_page_builder_menu_item_displayed)
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.masonry_card_collections) == 1)
        wait.until(lambda condition: len(new_custom_page.user_facing_modules.sidebar_menus) == 1)
        for card in new_custom_page.user_facing_modules.masonry_card_collections[0].cards:
            card.card_tags[0].get_attribute("textContent").should.equal(taxonomies_terms[0])
            card.card_tags[1].get_attribute("textContent").should.equal(taxonomies_terms[1])

        # Deleting the uploaded images from the media library
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_ONE_TITLE, Keys.RETURN)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, ElementNotInteractableException])
        wait.until(lambda condition: len(media_library_page.rows) == 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        wait.until(lambda condition: len(media_library_page.rows) == 0)
        media_library_page.search_input.clear()
        media_library_page.search_input.send_keys(IMAGE_TWO_TITLE, Keys.RETURN)
        wait.until(lambda condition: len(media_library_page.rows) == 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert.accept()
        wait.until(lambda condition: len(media_library_page.rows) == 0)

        # Deleting the created custom cards
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(CUSTOM_CARD_ONE_INFO['title'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_CARD_ONE_INFO['title'])
        all_cards_page.rows[0].hover_on_title()
        wait.until(lambda condition: all_cards_page.rows[0].delete.is_displayed())
        click(all_cards_page.rows[0].delete)
        all_cards_page.rows.should.be.empty
        all_cards_page.open()
        all_cards_page.search_input.send_keys(CUSTOM_CARD_TWO_INFO['title'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_CARD_TWO_INFO['title'])
        all_cards_page.rows[0].hover_on_title()
        wait.until(lambda condition: all_cards_page.rows[0].delete.is_displayed())
        click(all_cards_page.rows[0].delete)
        all_cards_page.rows.should.be.empty

        # Deleting the created page
        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_input.send_keys(CUSTOM_PAGE_TITLE)
        all_pages_page.search_button.click()
        all_pages_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        wait.until(lambda condition: all_pages_page.rows[0].delete.is_displayed())
        click(all_pages_page.rows[0].delete)
        all_pages_page.rows.should.be.empty
