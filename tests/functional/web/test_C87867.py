import pytest
import allure
import sure
import configuration.user
import configuration.system
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, StaleElementReferenceException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_new_custom_page import CreateNewCustomPage
from pages.web.page_builder import PageBuilderPage
from pages.web.components.page_builder.page_builder_modules import SideBarMenu, RowModules
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from utils.selenium_helpers import click
from mimesis import Text


PAGE = 'bibliocommons-settings'
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87867: PB - Sidebar Menu")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87867", "TestRail")
class TestC87867:
    def test_C87867(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as Implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()
        self.driver.delete_all_cookies()

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        # Creating new Custom Page
        create_new_page = CreateNewCustomPage(self.driver, configuration.system.base_url_web, post_type='page', page_type='custom').open()
        create_new_page.heading.text.should.match("Add New Page")
        create_new_page.title.send_keys(PAGE_TITLE)
        click(create_new_page.publish)
        create_new_page.wait.until(lambda s: create_new_page.is_success_message_displayed)
        create_new_page.is_success_message_displayed.should.be.true

        page_builder_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_builder_page.wpheader.page_builder.click()
        if page_builder_page.builder_panel.is_panel_visible is False:
            page_builder_page.page_builder.add_content.click()
        page_builder_page.builder_panel.modules_tab.click()
        page_builder_page.builder_panel.rows_tab.click()

        # Adding 2 Columns
        ActionChains(self.driver).drag_and_drop(page_builder_page.builder_panel.two_columns,
                                                page_builder_page.page_builder.body).perform()
        wait = WebDriverWait(self.driver, 10, poll_frequency=2,
                             ignored_exceptions=[NoSuchElementException, IndexError, StaleElementReferenceException,
                                                 ElementNotInteractableException])
        two_columns = RowModules(page_builder_page)
        wait.until(lambda condition: two_columns.column_body(0).is_displayed())
        page_builder_page.builder_panel.modules_tab.click()
        wait.until(lambda condition: page_builder_page.builder_panel.sidebar_menu.is_displayed())

        # Adding Sidebar menu to Column 1
        page_builder_page.builder_panel.scroll_to_bottom()
        ActionChains(self.driver).drag_and_drop(page_builder_page.builder_panel.sidebar_menu,
                                                two_columns.column_body(0)).perform()
        sidebar = SideBarMenu(page_builder_page)
        sidebar.content_tab.click()
        sidebar.wait.until(lambda s: sidebar.is_sidebar_placeholder_displayed)
        sidebar.is_sidebar_placeholder_displayed.should.be.true
        sidebar.content_tab_contents.select_a_menu.click()
        sidebar.content_tab_contents.sidebar_menu_results(0).click()
        sidebar_menu_name = sidebar.content_tab_contents.sidebar_menu_results(0).text
        sidebar.save.click()
        wait.until(lambda condition: len(page_builder_page.user_facing_modules.sidebar_menus) == 1)
        sidebar.sidebar_menu_name.get_attribute("innerHTML").should.contain(sidebar_menu_name)
        sidebar.is_sidebar_placeholder_displayed.should.be.false
        page_builder_page.page_builder.done.click()
        page_builder_page.wait.until(lambda condition: page_builder_page.page_builder.publish.is_displayed())
        page_builder_page.page_builder.publish.click()
        page_builder_page.wait.until(lambda s: page_builder_page.wpheader.page_builder.is_displayed())
        wait.until(lambda condition: len(page_builder_page.user_facing_modules.sidebar_menus) == 1)
        for sidebar_link in page_builder_page.user_facing_modules.sidebar_menus[0].sidebar_links:
            wait.until(lambda condition: sidebar_link.is_displayed())
        self.driver.refresh()
        wait.until(lambda condition: len(page_builder_page.user_facing_modules.sidebar_menus) == 1)
        for sidebar_link in page_builder_page.user_facing_modules.sidebar_menus[0].sidebar_links:
            wait.until(lambda condition: sidebar_link.is_displayed())
        sidebar_link.is_displayed().should.be.true

        # Deleting the created page
        all_pages = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages.page_builder_filter.click()
        all_pages.search_input.send_keys(PAGE_TITLE)
        all_pages.search_button.click()
        all_pages.rows[0].title.get_attribute("textContent").should.match(PAGE_TITLE)
        all_pages.rows[0].hover_on_title()
        all_pages.rows[0].delete.click()
        all_pages.rows.should.be.empty