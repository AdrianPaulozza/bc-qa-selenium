import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.recent_arrivals import RecentArrivalsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46958: New Titles -> Refine by Format or other facet")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46958", "TestRail")
class TestC46958:
    def test_C46958(self):
        self.base_url = "https://epl.demo.bibliocommons.com"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.browse.click()
        home_page.header.new_titles.click()
        recent_arrivals_page = RecentArrivalsPage(self.driver)
        recent_arrivals_page.select_from_dropdown(list = 'format', value = 'DVD')
        recent_arrivals_page.select_from_dropdown(list = 'content', value = 'Undetermined')
        recent_arrivals_page.select_from_dropdown(list = 'audience', value = 'Children')
        recent_arrivals_page.select_from_dropdown(list = 'language', value = 'French')
        recent_arrivals_page.apply.click()
        recent_arrivals_page.wait.until(lambda no_results_found: recent_arrivals_page.is_no_matching_results_displayed)
        recent_arrivals_page.no_matching_results.text.should.equal("The library has not acquired any items that meet your criteria recently.")
