import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47055: Pinned Search")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47055", "TestRail")
class TestC47055:
    def test_C47055(self):
        home_page = HomePage(self.driver, "https://chipublib.demo.bibliocommons.com/v2").open()
        search_results_page = home_page.header.search_for("harry potter")
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.filter_search_results("Paperback").click()
        first_search = search_results_page.search_result_items[0].bib_title.text
        search_results_page.wait.until(lambda s: search_results_page.active_filter_toggle_disabled.is_displayed())
        search_results_page.active_filter_toggle_disabled.click()
        search_results_page.header.clear_search()
        search_results_page.header.search_for("twilight")
        search_results_page.wait.until(lambda s: search_results_page.search_result_items[0].bib_title.text != first_search)
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.wait.until(lambda s: search_results_page.active_filter_toggle_enabled.is_displayed())
        for item in search_results_page.search_result_items:
            item.item_format.text.should.equal("Paperback")
        len(search_results_page.search_result_items).should.equal(10)
