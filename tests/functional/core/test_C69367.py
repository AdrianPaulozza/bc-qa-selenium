import pytest
import allure
import sure
import sys
sys.path.append('tests')

from pages.core.home import HomePage
from pages.core.v2.shelves import ShelvesPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69367: Find Available Titles button")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69367", "TestRail")
class TestC69367:
    def test_C69367(self):
        # Log into an account that has 1) a shelf with some items and 2) a shelf with no items
        # Verify that the "Find Available Titles" button (when togglz'ed on) shows on the shelf with items
        # Verify this button does not show on the shelf without items
        self.name ="21817002369272"
        self.password = "1992"
        base_url = "https://coaldale.demo.bibliocommons.com"
        home_page = HomePage(self.driver, base_url).open()
        home_page.header.log_in(self.name, self.password)
        home_page.header.login_state_user_logged_in.click()

        # Go to shelf with items
        home_page.header.for_later_shelf.click()
        shelves_page = ShelvesPage(self.driver)

        # Verify presence of  "Find Available Titles" button
        shelves_page.wait.until(lambda s: shelves_page.is_find_available_button_displayed)

        # Switch to empty shelf, verify absence of Find Available Titles
        shelves_page.wait.until(lambda s: shelves_page.loaded)
        shelves_page.in_progress_tab.click()
        shelves_page.wait.until(lambda s: shelves_page.loaded)
        shelves_page.wait.until(lambda s: shelves_page.is_current_shelf_in_progress)
        (shelves_page.is_find_available_button_displayed).should.be.false
