import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from utils.bc_test_connector import TestConnector

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C45996: Add item to shelves (Logged in)")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C45996", "TestRail")
class TestC45996:
    def test_C45996(self):
        self.user = TestConnector.User().create(self.driver)

        home_page = HomePage(self.driver, configuration.system.base_url).open()
        home_page.header.log_in(self.user.barcode, self.user.pin)
        search_results_page = home_page.header.search_for("Mandela") # , advanced_search = True)
        # Wait for search results to be greater than 0:
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.search_result_items[0].add_to_shelf_combo_button.click()
        # Wait for the success message to appear:
        # search_results_page.wait.until(lambda s: search_results_page.search_result_items[0].is_add_to_shelf_success_message_displayed)
        # success_message = search_results_page.search_result_items[0].add_to_shelf_success_message.text
        search_results_page.wait.until(lambda s: search_results_page.search_result_items[0].is_on_shelf_button_displayed)
        search_results_page.search_result_items[0].on_shelf_button.click()
        search_results_page.search_result_items[0].on_shelf_dropdown_button.click()
        search_results_page.search_result_items[0].remove_from_shelves.click()
        # Wait for the successfully removed message to appear:
        search_results_page.wait.until(lambda s: search_results_page.search_result_items[0].is_add_to_shelf_success_message_displayed)
        # Assert that the success message text correctly appeared:
        # success_message.should.match(r".+ has been added to your For Later shelf and shared")
